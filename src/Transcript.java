package src;

import java.sql.*;
import java.util.Scanner;

public class Transcript {
    public static void main(String args[]){

        Scanner in = new Scanner(System.in);
        System.out.println("Enter Oracle user id:");
        String userid = in.next();
        System.out.println("Enter Oracle password dbcourse:");
        String password = in.next();
        //catch any possible exception when logging in
        try{
            //get connection
            Class.forName("oracle.jdbc.driver.OracleDriver");
            //Class.forName("ojdbc7");
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@edgar0.cse.lehigh.edu:1521:cse241", userid, password);
            Statement stmt = con.createStatement();

            //search for entered substring
            System.out.println("Enter a substring to search");
            String sub;
            ResultSet nameResult = null;
            while(nameResult == null){
                try{
                    //SQL statement stmt.execute****
                    sub = in.next();
                    nameResult = stmt.executeQuery(
                            "select id, name " +
                                    "from student " +
                                    "where name like '%" + sub + "%'");
                }catch(SQLException sqle){ //test if query has this exception when found none
                    System.out.println("No matches. Try again." + sqle);
                }
            }
            System.out.println("Here is a list of all matching IDs");
            while(nameResult.next()){
                System.out.println(nameResult.getInt(1) + " " + nameResult.getString(2));
            }

            // check if entered id is valid
            // test this fully: non-int, out-of-range-int, hasnext works with next?
            System.out.println("Enter the student ID for the student whose transcript you seek.");
            int idSearch = -1;
            String name = "";
            ResultSet transcript = null;
            do{
                try{
                    System.out.println("Please enter an integer between 0 and 99999: ");
                    idSearch = Integer.parseInt(in.next());
                    if(idSearch >= 0 && idSearch <= 99999){
                        transcript = stmt.executeQuery(
                                "select year, semester, course.dept_name, takes.course_id, title, grade, student.name " +
                                        "from takes, student, course " +
                                        "where student.id = takes.id and takes.course_id = course.course_id and student.id = " + idSearch + " " + 
                                        "order by year, semester desc, course_id "
                        );
                    }
                }catch(NumberFormatException ex){
                    continue;
                }catch(SQLException sqle){
                    continue;
                }
            }while(transcript == null);

            transcript.next();
            name = transcript.getString(7);
            System.out.println("src.Transcript for student " + idSearch + " " + name);
            do{
                System.out.printf("%-30s%-50s%-2s\n", transcript.getInt(1) + " " +
                        transcript.getString(2) + " " + transcript.getString(3),
                        transcript.getInt(4) + " " + transcript.getString(5),
                        transcript.getString(6));
            }
            while(transcript.next());



            stmt.close();
            con.close();
        }catch (Exception sqle){
            System.out.print("Execption: " + sqle);
        }
    }
}
