Project name: Alset Eccentric Vehicle Database
Author: Desai Xie, Lehigh University, Class of 2020

This project can be run by connecting to another database (currently it uses the database provided by course insturctor), recreate tables according to relationalDesign.txt, and run PopulateRandomData.java to generate data to fill the empty tables.

Data Generation:
The data read in from the .txt files are from randomlists.com and simplemaps.com. Other data are generated in the code PopulateRandomData.java by myself.

Database.java:
This file contains highly reusable, well factored code with library methods that uses SQL PreparedStatament to interact with oracle database, and static inner classes representing various return types of the methods, and well formated toString methods for tidy output.

Two interfaces:
The two interfaces, i.e. customer interface and employee interface, all have features including repeatingly ask for input until valid input, discards updates to database that causes SQLException, in which case the program is still running, and user is asked to "report the following error message to techinal assistant", double-checking important decisions such as buying/selling vehicles, provides options for users before asking for input.

Customer Interface:
Login:
    Customer login is not implemented. It's assumed that when the customer enters their userId, they are identified and logged in successfully.
Profile:
    Show customer id, name, email, payment methods(cards). Customer can modify their profile.
My vehicles:
    Show all vehicles currently owned by this customer with vId, typeId, needMaintain, salePrice, boughtPrice.
ServiceLocations:
    Show all Alset serviceLocations with their handelModels and their addresses.
Types Listing:
    Show all types of Alset vehicles with their prices that are offered at any service location, or at a specific serviceLocation.
Buy new vehicle:
    Discount is currently not offered, since Black Friday just passed.
    Select model, select options, select serviceLocation to deliver. The vehicle will be in newDelivery of the serviceLocation, and will stay 'New' until the customer picks it up.
Take ordered vehicle:
    Select service location, then customer can take their ordered new Alset vehicle.
Request service visit:
    Customer selects one of their owned vehicles, and one of the serviceLocations that handles corresponding model. Alset will send repair staff to pickup the vehicle, examine its condition and then give description and evaluate cost, which are then entered in service_visit to fill the partially filled tuple.
Sell used:
    Put vehicle at a serviceLocation and Alset helps customer to sell the used vehicle.
    Notice: Table used_inventory is added a column sale_price number(9,2) after the due of relational design and data. Therefore most tuples in used_inventory have sale_price of 99999.99.
Buy used:
    Show all vehicles in usedInvetory in all serviceLocations or at a specific serviceLocation.


Service location Interface (For Alset employee who works at service locations):
Service location id:
    The interface asks for the service location every time. This won't happen in reality since on terminals located at individual service location it would record the sId of that service location.
Employee login:
    Again, login is not implemented as customer interface. There's no entering of password yet it's assumed that it is the employee themselves who are logging in. This employee id is not used.
Used Inventory:
    Show all vehicles in the used inventory.
    Notice: Table used_inventory is added a column sale_price number(9,2) after the due of relational design and data. Therefore most tuples in used_inventory have sale_price of 99999.99.
Recall:
    Show recalled Alset vehicles that are at this service location. Storage manager can remove them from database record after Alset headquarter come to collect them.
Show room:
    Check all vehicles in current service location.
New delivery:
    Show all new vehicles ordered by customers that are to be picked up at this service location.
Service Visit:
    Show all finished service visit records at this service location.
    Notice: Most service visits have description 'This description is not null' since it's hard to randomly generate descriptions for them when populating my tables.
Handel service visit:
    Check the condition of the vehicle come in, and enter a description and estimate the cost to the record of this service visit.


Other interfaces not implemented but would exist in the real system:
An CEO interface that can add new vehicle types, produce new vehicles, constructe new service locations, etc.
