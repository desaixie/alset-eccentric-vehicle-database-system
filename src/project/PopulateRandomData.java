/*
Project name: Alset Eccentric Vehicle Database
Author: Desai Xie, Lehigh University, Class of 2020
 */
package project;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;


public class PopulateRandomData {
    private static ArrayList<Integer> M = new ArrayList<>();
    private static ArrayList<Integer> U = new ArrayList<>();
    private static ArrayList<Integer> S = new ArrayList<>();
    private static ArrayList<Integer> K = new ArrayList<>();
    private static ArrayList<Vehicle> Used = new ArrayList<>();
    private static ArrayList<Vehicle> New = new ArrayList<>();
    private static HashMap<Integer, Integer> currentOwner = new HashMap<>();

    private static class Vehicle {
        private int vId;
        private String model;
        private int year;
        private double salePrice;
        private Vehicle(int vId, String model, int year, double salePrice) {
            this.vId = vId;
            this.model = model;
            this.year = year;
            this.salePrice = salePrice;
        }
    }

    private static boolean populateCustomerHasCardLivesIn(Database db) {
        // Read in names and emails from file
        Scanner inNames = null;
        Scanner inEmails = null;
        Scanner inCities = null;
        Scanner inStreets = null;
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> emails = new ArrayList<>();
        ArrayList<String> cities = new ArrayList<>();
        ArrayList<String> countries = new ArrayList<>();
        ArrayList<String> streets = new ArrayList<>();
        try {
            inNames = new Scanner(new File("C:\\zong\\Lehigh\\Study\\c2018Fall\\CSE341\\FinalProject\\names.txt"));
            inEmails = new Scanner(new File("C:\\zong\\Lehigh\\Study\\c2018Fall\\CSE341\\FinalProject\\emails.txt"));
            inCities = new Scanner(new File("C:\\zong\\Lehigh\\Study\\c2018Fall\\CSE341\\FinalProject\\cities.txt"));
            inStreets = new Scanner(new File("C:\\zong\\Lehigh\\Study\\c2018Fall\\CSE341\\FinalProject\\streets.txt"));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        int j = 0;
        while (inNames.hasNextLine()) {
            String in = inNames.nextLine();
            names.add(in);
            j++;
        }
        System.out.println("Read" + j + " names.");
        j = 0;
        while (inEmails.hasNextLine()) {
            emails.add(inEmails.nextLine());
            j++;
        }
        System.out.println("Read" + j + " emails.");
        j = 0;
        while (inCities.hasNextLine()) {
            String[] cityCountry = inCities.nextLine().split("\t");
            cities.add(cityCountry[0]);
            countries.add(cityCountry[1]);
            j++;
        }
        System.out.println("Read" + j + " cities and countries.");
        j = 0;
        int y = 0;
        while (inStreets.hasNextLine()) {
            if (y == 0) {
                streets.add(inStreets.nextLine());
                y = 1;
                j++;
            } else {
                inStreets.nextLine();
                y = 0;
            }
        }
        System.out.println("Read" + j + " streets.");
        inNames.close();
        inEmails.close();
        inCities.close();
        inStreets.close();


        // Generate random card numbers and types
        ArrayList<Long> cardNumbers = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            cardNumbers.add((long) (Math.random() * 9000000000000000L) + 1000000000000000L);
        }
        // Generate random addresses
        String[] cTypes = {"Credit", "Debit"};
        String[] planets = {"Earth", "Mars", "Moon"};
        for (int i = 0; i < 1000; i++) {
            String name = names.remove((int) (Math.random() * names.size()));
            String email = emails.remove((int) (Math.random() * emails.size()));
            long cardNumber = cardNumbers.remove((int) (Math.random() * cardNumbers.size()));
            String cType = cTypes[(int) (Math.random() * 2)];
            String planet = planets[(int) (Math.random() * 3)];
            int x = (int) (Math.random() * countries.size());  // Makes sure country and city are from the same row
            String country = countries.get(x);
            String city = cities.get(x);
            String street = streets.remove((int) (Math.random() * streets.size()));
            int id = db.insertCustomer(name, email);
            if (id == -1) {
                System.err.println("Inserting customer failed. Stopping...");
                return false;
            }
            db.insertHasCard(id, cardNumber, cType);
            db.insertLivesIn(id, planet, country, city, street);
        }

        System.out.println("Inserting Customer, HasCard, LivesIn done");
        return true;
    }

    private static boolean populateVTypeVehicleElse(Database db) {
        // options: auto-pilot, crater-descent, hyper-speed, intelligent-guide, laser-gun, oxygen-storage, 3D-transparency, wormhole-travel,
        // 200 Model K
        db.insertVType("K", 2012, "", 50000.00);
        produceVehicles(db, "K", 2012, "", 50000.00, 50);
        db.insertVType("K", 2013, "a", 55000.00);
        produceVehicles(db, "K", 2013, "a", 55000.00, 30);
        db.insertVType("K", 2012, "h", 60000.00);
        produceVehicles(db, "K", 2012, "h", 60000.00, 50);
        db.insertVType("K", 2013, "ah", 64000.00);
        produceVehicles(db, "K", 2013, "ah", 640000.00, 20);
        db.insertVType("K", 2014, "ahi", 69000.00);
        produceVehicles(db, "K", 2014, "ahi", 69000.00, 50);

        // 100 Model U
        db.insertVType("U", 2013, "", 80000.00);
        produceVehicles(db, "U", 2013, "", 80000.00, 10);
        db.insertVType("U", 2014, "ai", 90000.00);
        produceVehicles(db, "U", 2014, "ai", 90000.00, 30);
        db.insertVType("U", 2016, "o", 80800.00);
        produceVehicles(db, "U", 2016, "o", 80800.00, 10);
        db.insertVType("U", 2014, "ahi", 99000.00);
        produceVehicles(db, "U", 2014, "ahi", 99000.00, 20);
        db.insertVType("U", 2017, "aio", 90500.00);
        produceVehicles(db, "U", 2017, "aio", 90500.00, 10);
        db.insertVType("U", 2017, "ahio", 99400.00);
        produceVehicles(db, "U", 2017, "ahio", 99400.00, 10);
        db.insertVType("U", 2018, "ahiot", 120000.00);
        produceVehicles(db, "U", 2018, "ahiot", 120000.00, 10);

        // 300 Model M
        db.insertVType("M", 2015, "", 90000.00);
        produceVehicles(db, "M", 2015, "", 90000.00, 10);
        db.insertVType("M", 2015, "ai", 100000.00);
        produceVehicles(db, "M", 2015, "ai", 100000.00, 40);
        db.insertVType("M", 2017, "o", 90800.00);
        produceVehicles(db, "M", 2017, "o", 90800.00, 30);
        db.insertVType("M", 2016, "ahi", 109000.00);
        produceVehicles(db, "M", 2016, "ahi", 109000.00, 20);
        db.insertVType("M", 2017, "ahio", 100500.00);
        produceVehicles(db, "M", 2017, "ahio", 100500.00,50);
        db.insertVType("M", 2018, "ahiot", 130000.00);
        produceVehicles(db, "M", 2018, "ahiot", 130000.00, 100);
        db.insertVType("M", 2019, "ahiotw", 330000.00);
        produceVehicles(db, "M", 2019, "ahiotw", 330000.00, 50);

        // 400 Model S
        db.insertVType("S", 2017, "", 125000.00);
        produceVehicles(db, "S", 2017, "", 125000.00, 30);
        db.insertVType("S", 2017, "ai", 135000.00);
        produceVehicles(db, "S", 2017, "ai", 135000.00, 60);
        db.insertVType("S", 2017, "o", 125800.00);
        produceVehicles(db, "S", 2017, "o", 125800.00, 20);
        db.insertVType("S", 2017, "c", 140000.00);
        produceVehicles(db, "S", 2017, "c", 140000.00, 10);
        db.insertVType("S", 2017, "co", 140500.00);
        produceVehicles(db, "S", 2017, "co", 140500.00, 140);
        db.insertVType("S", 2017, "achio", 150500.00);
        produceVehicles(db, "S", 2017, "achio", 150500.00, 35);
        db.insertVType("S", 2018, "achiot", 165000.00);
        produceVehicles(db, "S", 2018, "achiot", 165000.00, 15);
        db.insertVType("S", 2019, "achiotw", 365000.00);
        produceVehicles(db, "S", 2019, "achiotw", 365000.00, 40);
        db.insertVType("S", 2019, "achilot", 365000.00);
        produceVehicles(db, "S", 2019, "achilot", 365000.00, 20);
        db.insertVType("S", 2019, "achilotw", 565000.00);
        produceVehicles(db, "S", 2019, "achilotw", 565000.00, 30);

        // Total 1000 vehicles. 200 K, 100 U, 300 M, 400 S. 0.15 new, 0.85 used: 0.1 need maintain.

        System.out.println("Inserting Vehicles and VTypes done.");
        return true;
    }


    private static boolean populateServiceLocation(Database db) {
        // Populate ServiceLocations

        int sId;
        sId = db.insertServiceLocation("KUS", "Earth", "United States", "New York", "1 Edgewater St.");
        K.add(sId);
        U.add(sId);
        S.add(sId);
        sId = db.insertServiceLocation("KUS", "Earth", "United States", "San Francisco", "10 The Embarcadero");
        K.add(sId);
        U.add(sId);
        S.add(sId);
        sId = db.insertServiceLocation("KU", "Earth", "China", "Shanghai", "28-1 Suitang Hwy.");
        K.add(sId);
        U.add(sId);
        sId = db.insertServiceLocation("KUS", "Earth", "Spain", "Barcelona", "5 Av. del Parallel");
        K.add(sId);
        U.add(sId);
        S.add(sId);
        sId = db.insertServiceLocation("K", "Earth", "United States", "Austin", "6 W 10th St.");
        K.add(sId);
        sId = db.insertServiceLocation("KS", "Earth", "China", "Beijing", "11 Chengfu Rd.");
        K.add(sId);
        S.add(sId);
        sId = db.insertServiceLocation("MS", "Moon", "Moon US", "New Boston", "1 Alset St.");
        M.add(sId);
        S.add(sId);
        sId = db.insertServiceLocation("MS", "Moon", "Moon Germany", "New Hamburg", "1 Alset St.");
        M.add(sId);
        S.add(sId);
        sId = db.insertServiceLocation("M", "Moon", "Moon China", "Xinjing", "1 Alset St.");
        M.add(sId);
        sId = db.insertServiceLocation("S","Mars", "Mars US", "New Los Angeles", "1 Alset St.");
        S.add(sId);
        sId = db.insertServiceLocation("S", "Mars", "Mars Russia", "Moscow", "1 Alset St.");
        S.add(sId);

        System.out.println("Inserting ServiceLocation done.");
        return true;
    }

    public static void produceVehicles(Database db, String model, int year, String options, double salePrice, int amount){
        String typeId = model + year + options;
        for(int i = 0; i < amount; i ++) {
            String saleHistory = (Math.random() < 0.15) ? "New" : "Used";
            String needMaintain = (saleHistory.equals("Used")) ? (Math.random() < 0.1 ? "Y" : "N") : "N";
            int vid = db.insertVehicle(typeId, needMaintain, saleHistory);
            if (vid == -1) {
                System.err.println("Inserting vehicle failed. ");
                return;
            }
            if (saleHistory.equals("Used")) {
                Used.add(new Vehicle(vid, model, year, salePrice));
            } else {
                New.add(new Vehicle(vid, model, year, salePrice));
            }
        }
    }

    private static boolean populateWithUsedNew(Database db) {
        // Populate used cars into owns, recall, used_inventory
        for(int i = 0; i < Used.size(); i ++){
            int vid = Used.get(i).vId;
            String model = Used.get(i).model;
            double salePrice = Used.get(i).salePrice;

            if(Math.random() < 0.95) {
                // Populate owns
                String status = (Math.random() < 0.2) ? "Past" : "Current";
                DecimalFormat df = new DecimalFormat("#.##");
                double boughtPrice = (Math.random() < 0.6) ? Double.valueOf(df.format(salePrice - Math.random() * 1000.0)) : salePrice;  // Possible discount
                int id1 = (int) (Math.random() * 1000) + 1;
                db.insertOwn(id1, vid, boughtPrice, status);

                if (status.equals("Past")) {  // Past ownership means there must be other ownerships, at least one "Current"
                    int id2 = (int) (Math.random() * 1000) + 1;
                    String status2 = (Math.random() < 0.2) ? "Past" : "Current";
                    while (id2 == id1) {  // Preventing cars resold to same customer
                        id2++;
                    }
                    double resalePrice2 = ((Math.random() * 0.1) + 0.6) * boughtPrice;
                    db.insertOwn(id2, vid, resalePrice2, status2);

                    if (status2.equals("Past")) {
                        int id3 = (int) (Math.random() * 1000) + 1;
                        while (id3 == id1 || id3 == id2) {  // Preventing cars resold to same customer
                            id3++;
                        }
                        double resalePrice3 = ((Math.random() * 0.1) + 0.6) * resalePrice2;
                        db.insertOwn(id3, vid, resalePrice3, "Current");// Maximum 2 reselling
                        currentOwner.put(vid, id3);
                    } else {
                        currentOwner.put(vid, id2);
                    }
                } else {
                    currentOwner.put(vid, id1);
                }
            } else { // Other used cars are either recalled or in used_inventory
                // recalls are dealt later
                switch(model){
                    case "M": db.insertUsedInventory(M.get((int)(Math.random() * M.size())), vid, 99999.99); break;
                    case "U": db.insertUsedInventory(U.get((int)(Math.random() * U.size())), vid, 99999.99); break;
                    case "S": db.insertUsedInventory(S.get((int)(Math.random() * S.size())), vid, 99999.99); break;
                    case "K": db.insertUsedInventory(K.get((int)(Math.random() * K.size())), vid, 99999.99); break;
                }
            }
        }
        // Populate new cars into showroom, new_delivery
        for(int i = 0; i < New.size(); i ++){
            int vid = New.get(i).vId;
            String model = New.get(i).model;

            if(Math.random() < 0.3){  // showroom
                switch(model){
                    case "M": db.insertShowroom(M.get((int)(Math.random() * M.size())), vid); break;
                    case "U": db.insertShowroom(U.get((int)(Math.random() * U.size())), vid); break;
                    case "S": db.insertShowroom(S.get((int)(Math.random() * S.size())), vid); break;
                    case "K": db.insertShowroom(K.get((int)(Math.random() * K.size())), vid); break;
                }

            } else {  // new_delivery
                int id = (int)(Math.random() * 1000) + 1;
                switch(model){
                    case "M": db.insertNewDelivery(M.get((int)(Math.random() * M.size())), vid, id); break;
                    case "U": db.insertNewDelivery(U.get((int)(Math.random() * U.size())), vid, id); break;
                    case "S": db.insertNewDelivery(S.get((int)(Math.random() * S.size())), vid, id); break;
                    case "K": db.insertNewDelivery(K.get((int)(Math.random() * K.size())), vid, id); break;
                }

            }
        }
        System.out.println("Inserting own, used_inventory, showroom, new_delivery");

        return true;
    }

    private static boolean populateServiceVisit(Database db){
        for(int j = 0; j < 300; j ++){
            Vehicle x = Used.get((int)(Math.random() * Used.size()));
            int vid = x.vId;
            if(!currentOwner.containsKey(vid)){
                continue;
            }
            int id = currentOwner.get(vid);
            int year = x.year;
            String description = "";
            String date = (int)(Math.random() * 12 + 1) + "\\" + (int)(Math.random() * 28 + 1) + "\\" + (int)(Math.random() * (2020 - year) + year);
            double cost = Math.random() * 10000.0 + 200.0;
            switch(x.model){
                case "M": db.insertServiceVisit(id, vid, M.get((int)(Math.random() * M.size())), description, date, cost); break;
                case "U": db.insertServiceVisit(id, vid, U.get((int)(Math.random() * U.size())), description, date, cost); break;
                case "S": db.insertServiceVisit(id, vid, S.get((int)(Math.random() * S.size())), description, date, cost); break;
                case "K": db.insertServiceVisit(id, vid, K.get((int)(Math.random() * K.size())), description, date, cost); break;
            }
        }
        return true;
    }

    private static boolean populateRecall(Database db){

        return true;
    }

    public static void main(String[] args){
        Database db = Database.getDatabase();
//        if(!populateCustomerHasCardLivesIn(db))
//        {
//            db.disconnect();
//            System.exit(1);
//        }
//        if(!populateServiceLocation(db))
//        {
//            db.disconnect();
//            System.exit(1);
//        }
//        if(!populateVTypeVehicleElse(db))
//        {
//            db.disconnect();
//            System.exit(1);
//        }
//        if(!populateWithUsedNew(db))
//        {
//            db.disconnect();
//            System.exit(1);
//        }
//        if(!populateServiceVisit(db))
//        {
//            db.disconnect();
//            System.exit(1);
//        }
        ArrayList<Integer> y = new ArrayList<>();
        y.add(1);
        y.add(2);
        y.add(3);
        y.add(4);
        y.add(5);
        y.add(6);
        db.recall("K", 2013, "a", y);
    }
}
