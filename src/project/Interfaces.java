package project;

import java.util.Scanner;

public class Interfaces {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        while(true) {
            System.out.println("Please enter 'Customer' for using customer interface or 'Employee' for employee interface.");
            if (in.next().equals("Customer")) {
                CustomerInterface.interFace();
                break;
            } else if (in.next().equals("Employee")) {
                EmployeeInterface.interFace();
                break;
            } else {
                System.out.println("Invalid input.");
            }
        }
    }
}
