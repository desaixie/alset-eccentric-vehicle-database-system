/*
Project name: Alset Eccentric Vehicle Database
Author: Desai Xie, Lehigh University, Class of 2020
 */
package project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class CustomerInterface {
    /**
     * Print the menu for our program
     */
    static void menu() {
        System.out.println("Enter the number twice for detailed description, once for choosing the option. ");
        System.out.println("  [1] Profile");
        System.out.println("  [2] My Vehicles");
        System.out.println("  [3] Service Locations");
        System.out.println("  [4] Types Listing");
        System.out.println("  [5] Buy New");
        System.out.println("  [6] Take Ordered Vehicle");
        System.out.println("  [7] Request Service Visit");
        System.out.println("  [8] Sell Used");
        System.out.println("  [9] Buy Used");
        System.out.println("  [q] Quit Program");
        System.out.println("  [?] Help (this message)");
    }

    /**
     * Ask the user to enter a menu option; repeat until we get a valid option
     *
     * @param in A BufferedReader, for reading from the keyboard
     * @return The character corresponding to the chosen menu option
     */
    static char prompt(BufferedReader in) {
        // The valid actions:
        String actions = "123456789q?";

        // We repeat until a valid single-character option is selected
        while (true) {
            System.out.println("\nYour are at main menu.,");
            System.out.println("Enter an option:> ");
            String action;
            try {
                action = in.readLine();
            } catch (IOException e) {
                System.out.println("Couldn't access input from terminal.");
                continue;
            }
            if (action.length() > 2) {
                System.out.println("Invalid Command.");
                continue;
            }
            if(action.length() == 1){
                if (actions.contains(action)) {
                    return action.charAt(0);
                }
            }
            if(action.length() == 2){
                switch(action){
                    case "11":
                        System.out.println("Profile\nShow your id, name, email, payment methods(cards). You " +
                                "can modify your profile.");
                        break;
                    case "22":
                        System.out.println("My Vehicles\nShow all vehicles that you own with vehicle id, " +
                                "type id, if it needs maintain, the sale price of a new one, the price you " +
                                "bought it.");
                        break;
                    case "33":
                        System.out.println("Service Locations\nShow all Alset service locations with their " +
                                "handel models and their addresses.");
                        break;
                    case "44":
                        System.out.println("Types Listing\nShow all types of Alset vehicles with their prices " +
                                "that are offered at any service location, or at a specific service location.");
                        break;
                    case "55":
                        System.out.println("Buy New\nSelect your model, options, and a serviceLocation to " +
                                "deliver.");
                        break;
                    case "66":
                        System.out.println("Select service location, and reenter your id, then wait for our " +
                                "staff, and you can take your ordered new Alset vehicle.");
                        break;
                    case "77":
                        System.out.println("Request Service Visit\nSelects one of your vehicles, and one of " +
                                "the service locations that handles corresponding model. Alset will send " +
                                "repair staff to pickup the vehicle, examine its condition and then give " +
                                "description and evaluate cost.");
                        break;
                    case "88":
                        System.out.println("Sell Used\n Put your vehicle at a service location and Alset will " +
                                "help you to sell your vehicle.");
                        break;
                    case "99":
                        System.out.println("Buy Used\nShow all vehicles in used invetory of all service " +
                                "locations or at a specific serviceLocation.");
                        break;
                    case "qq":
                        System.out.println("Quit Program");
                        break;
                    case "??":
                        System.out.println("Help\nDisplay the options again.");
                        break;
                    default:
                        System.out.println("Invalid Command");
                        continue;
                }
                return prompt(in);
            }
        }
    }

    /**
     * Ask the user to enter a String message
     *
     * @param in      A BufferedReader, for reading from the keyboard
     * @param message A message to display when asking for input
     * @return The string that the user provided. May be "".
     */
    static String getString(BufferedReader in, String message) {
        String s;
        try {
            System.out.println(message + " :> ");
            s = in.readLine();
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("Couldn't access stdin");
            return "";
        }
        return s;
    }

    /**
     * Ask the user to enter an integer
     *
     * @param in      A BufferedReader, for reading from the keyboard
     * @param message A message to display when asking for input
     * @return The integer that the user provided. On error, it will be -1
     */
    static int getInt(BufferedReader in, String message) {
        int i = -1;
        try {
            System.out.println(message + " :> ");
            i = Integer.parseInt(in.readLine());
        } catch (IOException e) {
            System.out.println("Couldn't access stdin");
        } catch (NumberFormatException e) {
            System.out.println("Didn't enter an integer");
        }
        return i;
    }



    public static void interFace() {
        // Get a fully-configured connection to the database, or exit
        // immediately
        Database db = Database.getDatabase();
        if (db == null)
            return;

        // Start our basic command-line interpreter:
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Welcome to Alset Eccentric Vehicle's Customer Interface.\n");
        int id = -1;
        while(id == -1) {
            id = getInt(in, "Enter your user id");
            if(id < 1){
                System.out.println("This customer id doesn't exist. Please try again.");
                id = -1;
                continue;
            }
            if(db.selectOneCustomer(id) == null){
                System.out.println("This customer id doesn't exist. Please try again.");
                id = -1;
                continue;
            }
        }
        System.out.println("Login succeeds.");
        menu();
        while (true) {
            // Get the user's request
            char action = prompt(in);


            if (action == '1') {
                // Show profile
                Database.Customer customer = db.selectOneCustomer(id);
                ArrayList<Database.HasCard> cards = db.selectCards(id);
                Database.Address address = db.selectLivesIn(id);
                System.out.println("Profile\n");
                System.out.println("Id: " + customer.id);
                System.out.println("Name: " + customer.name);
                System.out.println("Email: " + customer.email);
                System.out.println("You have " + cards.size() + " card(s)");
                for (int i = 0; i < cards.size(); i++) {
                    Database.HasCard card = cards.get(i);
                    System.out.println(card);
                }
                System.out.println("Address: " + address);

                // Ask if customer wants to edit profile
                System.out.println("\nEdit your profile?");
                String edit = getString(in, "Enter 'Yes' or 'No'");
                if (edit.equals("Yes")){
                    while(edit.equals("Yes")){
                        System.out.println("You can edit one row at a time.");
                        String row = getString(in, "Enter 'Name' or 'Email' or 'Cards' or 'Address'");
                        if(row.equals("Name")){
                            String name = getString(in, "Enter new name");
                            if(db.editCustomer(id, name, null) == 0){
                                System.out.println("Edit succeeds.");
                            }
                            System.out.println("Continue editing?");
                            edit = getString(in, "Enter 'Yes' or 'No'");
                        } else if(row.equals("Email")){
                            String email = getString(in, "Enter new email");
                            if(db.editCustomer(id, null, email) == 0){
                                System.out.println("Edit succeeds.");
                            }
                            System.out.println("Continue editing?");
                            edit = getString(in, "Enter 'Yes' or 'No'");
                        } else if(row.equals("Cards")){
                            System.out.println("Add or remove card?");
                            String card = getString(in, "Enter 'Add', or 'Remove'");
                            if(card.equals("Add")){
                                long cNumber = Long.getLong(getString(in, "Enter card number"));
                                String cType = getString(in, "Enter card type");
                                if(db.insertHasCard(id, cNumber, cType) != -1){
                                    System.out.println("Add card succeeds");
                                }
                            } else if(card.equals("Remove")){
                                long cNumber = Long.getLong(getString(in, "Enter card number to remove"));
                                if(db.deleteHasCard(id, cNumber) != -1){
                                    System.out.println("Remove card succeeds");
                                }
                            } else {
                                System.out.println("Wrong input.");
                            }
                            System.out.println("Continue editing?");
                            edit = getString(in, "Enter 'Yes' or 'No'");
                        } else if(row.equals("Address")) {
                            String planet = getString(in, "Enter new planet");
                            String country = getString(in, "Enter new country");
                            String city = getString(in, "Enter new city");
                            String street = getString(in, "Enter new street");
                            if(db.editLivesIn(id, planet, country, city, street) == 0){
                                System.out.println("Edit succeeds.");
                            }
                            System.out.println("Continue editing?");
                            edit = getString(in, "Enter 'Yes' or 'No'");
                        } else {
                            System.out.println("Wrong input.");
                        }
                    }
                } else if (!edit.equals("No")) {
                    System.out.println("Wrong input.");
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '2') {  // "My vehicles
                ArrayList<Database.OwnedVehicle> vs = db.selectOwnedVehicles(id);
                if(vs.size() == 0){
                    System.out.println("You don't own any Alset vehicle currently.");
                } else {
                    System.out.println("\nYour vehicle(s)");
                    for (int i = 0; i < vs.size(); i++) {
                        System.out.println("\n" + vs.get(i));
                    }
                }


            } else if (action == '3') { // "Service Locations"
                ArrayList<Database.ServiceLocation> sls = db.selectAllServiceLocations();
                if(sls.size() == 0){
                    System.out.println("Sorry, Alset has closed all of its service locations.");
                } else {
                    for (int i = 0; i < sls.size(); i++) {
                        System.out.println(sls.get(i) + "\n");
                    }
                }


            } else if (action == '4') {  // Prices Listing
                int sId = getInt(in, "Enter service location id for vehicle types at specific service location, or 0 for all Alset vehicle types");
                if(sId == 0){
                    ArrayList<Database.VType> vtypes = db.selectAllVTypes();
                    System.out.println("Alset Vehicle types available :");
                    for (int i = 0; i < vtypes.size(); i++) {
                        System.out.println(vtypes.get(i));
                    }
                } else if (sId != -1){
                    Database.ServiceLocation sl = db.selectServiceLocation(sId);
                    String[] models = sl.handelModels.split("");
                    ArrayList<Database.VType> vtypes = new ArrayList<>();
                    for (int i = 0; i < models.length; i++) {
                        vtypes.addAll(db.selectVTypesWith(models[i]));
                    }
                    System.out.println("Alset Vehicle types available at service location " + sId + ": ");
                    for (int i = 0; i < vtypes.size(); i++) {
                        System.out.println(vtypes.get(i));
                    }
                } else {
                    System.out.println("Wrong input.");
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '5') { // Buy new vehicle
                String model = getString(in, "Enter the model you want, 'M', 'U', 'S', or 'K'");
                if (Arrays.asList(new String[] {"M", "U", "S", "K"}).contains(model)) {
                    ArrayList<Database.VType> vtypes = db.selectVTypesWith(model);
                    System.out.println("Select the vehicle type from the following:");
                    ArrayList<String> typeIds = new ArrayList<>();
                    for (int i = 0; i < vtypes.size(); i++) {
                        System.out.println(vtypes.get(i));
                        typeIds.add(vtypes.get(i).typeId);
                    }
                    String typeId = getString(in, "Enter the type id of your desired vehicle type");
                    if(typeIds.contains(typeId)){
                        Database.Vehicle v = db.selectNewVehicleWith(typeId);
                        if(v == null){
                            System.out.println("Sorry, the type you chose is sold out. Alset will be producing that type soon.");
                        } else {
                            int vId = v.vId;
                            System.out.println("Select service location to deliver your new Alset vehicle from the following: ");
                            ArrayList<Database.ServiceLocation> sls = db.selectServiceLocationsThatHandel(model);
                            for (int i = 0; i < sls.size(); i++) {
                                System.out.println(sls.get(i) + "\n");
                            }
                            int sId = getInt(in, "Enter service location id");
                            Database.ServiceLocation s = db.selectServiceLocation(sId);
                            if (s != null) {
                                System.out.println("You have discount: $0.00");
                                double salePrice = -1.00;  // This won't be -1 at the end, as checked by the if statement above
                                for (int i = 0; i < vtypes.size(); i++) {
                                    if (vtypes.get(i).typeId.equals(typeId)) {
                                        salePrice = vtypes.get(i).salePrice;
                                    }
                                }
                                System.out.println("The Alset vehicle that you just ordered will cost $" + salePrice);
                                System.out.println("Confirm your order?");
                                String yn = getString(in, "Enter 'Yes' or 'No'");
                                if (yn.equals("Yes")) {
                                    if(db.buyNewVehicle(id, vId, salePrice, sId) == 0){
                                        System.out.println("Congratulations! The the order is successful. Please pick it up at \n" + s);
                                        System.out.println("\n" + v);
                                    }
                                } else if (yn.equals("No")) {
                                    System.out.println("Your order is cancelled.");
                                } else {
                                    System.out.println("Wrong input.");
                                }
                            }
                        }
                    } else {
                        System.out.println("Wrong input.");
                    }
                } else {
                    System.out.println("Wrong input.");
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '6') {  // Take ordered vehicle
                while(true) {
                    int sId = getInt(in, "Please enter the service location id that you are at, or '0' for all service locations listing");
                    if (sId == 0) {
                        ArrayList<Database.ServiceLocation> sls = db.selectAllServiceLocations();
                        if (sls.size() == 0) {
                            System.out.println("Sorry, Alset has closed all of its service locations.");
                        } else {
                            for (int i = 0; i < sls.size(); i++) {
                                System.out.println(sls.get(i));
                            }
                        }
                        continue;
                    }
                    if (db.selectServiceLocation(sId) != null) {
                        int vId = db.selectNewDeliveredVId(id, sId);
                        db.setSaleHistoryUsed(vId);
                        System.out.println("Enjoy your ride with your new Alset vehicle. ");
                        break;
                    } else {
                        System.out.println("Wrong input.");
                        break;
                    }
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '7') {  // Request service visit
                System.out.println("Selet one of your owned vehicle(s) that need service visit");
                ArrayList<Database.OwnedVehicle> vs = db.selectOwnedVehicles(id);
                if(vs.size() == 0){
                    System.out.println("You don't own any Alset vehicle currently.");
                } else {
                    ArrayList<Integer> vIds = new ArrayList<>();
                    System.out.println("\nYour vehicle(s)");
                    for (int i = 0; i < vs.size(); i++) {
                        System.out.println("\n" + vs.get(i));
                        vIds.add(vs.get(i).vId);
                    }
                    int vId = getInt(in, "Enter your vehicle id");
                    if(vIds.contains(vId)){
                        for (int i = 0; i < vs.size(); i++) {
                            if(vs.get(i).vId == vId){  // Find the vehicle
                                String model = vs.get(i).model;
                                ArrayList<Database.ServiceLocation> sls = db.selectServiceLocationsThatHandel(model);
                                ArrayList<Integer> sIds = new ArrayList<>();
                                System.out.println("Select one of the following service locations that you would like your service to be at.");
                                for (int j = 0; j < sls.size(); j++) {
                                    System.out.println(sls.get(j) + "\n");
                                    sIds.add(sls.get(j).sId);
                                }
                                int sId = getInt(in, "Enter the service location id");
                                if(sIds.contains(sId)){
                                    System.out.printf("Confirm your service visit at service location id(%d) " +
                                            "with your vehicle id(%d)?\n", sId, vId);
                                    String yn = getString(in, "Enter 'Yes' or 'No'");
                                    if (yn.equals("Yes")) {
                                        int repairId = db.insertPartialServiceVisit(id, vId, sId);
                                        if(repairId != -1){
                                            System.out.printf("Your repairId is %d. Please wait for Alset staff " +
                                                            "to take your vehicle id(%d) to service location id(%d). " +
                                                            "Your vehicle will be then examined and given a " +
                                                            "description and an estimated cost.\n",
                                                    repairId, vId, sId);
                                        }
                                    } else if (yn.equals("No")) {
                                        System.out.println("Your service visit is cancelled.");
                                    } else {
                                        System.out.println("Wrong input.");
                                    }
                                } else {
                                    System.out.println("Wrong input.");
                                }
                            }
                        }
                    } else {
                        System.out.println("Wrong input.");
                    }
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '8') {  // Sell Used
                System.out.println("Selet one of your owned vehicle(s) that you want to sell");
                ArrayList<Database.OwnedVehicle> vs = db.selectOwnedVehicles(id);
                if(vs.size() == 0){
                    System.out.println("You don't own any Alset vehicle currently.");
                } else {
                    ArrayList<Integer> vIds = new ArrayList<>();
                    System.out.println("Your vehicle(s)");
                    for (int i = 0; i < vs.size(); i++) {
                        System.out.println("\n" + vs.get(i));
                        vIds.add(vs.get(i).vId);
                    }
                    int vId = getInt(in, "Enter your vehicle id");
                    if(vIds.contains(vId)){
                        for (int i = 0; i < vs.size(); i++) {
                            if (vs.get(i).vId == vId) {  // Find the vehicle
                                String model = vs.get(i).model;
                                ArrayList<Database.ServiceLocation> sls = db.selectServiceLocationsThatHandel(model);
                                ArrayList<Integer> sIds = new ArrayList<>();
                                System.out.println("Select one of the following service locations, or '0' " +
                                        "for a random one.");
                                for (int j = 0; j < sls.size(); j++) {
                                    System.out.println(sls.get(j) + "\n");
                                    sIds.add(sls.get(j).sId);
                                }
                                int sId = getInt(in, "Enter the service location id");
                                if(sId == 0){
                                    sId = sIds.get((int)(Math.random() * sIds.size()));
                                }
                                if (sIds.contains(sId)) {
                                    int price = getInt(in, "Enter the price you want to sell your used vehicle");
                                    System.out.printf("Confirm your sale of your used vehicle id(%d) at " +
                                            "service location id(%d) for $%.2f\n", vId, sId, (double)price);
                                    String yn = getString(in, "Enter 'Yes' or 'No'");
                                    if (yn.equals("Yes")) {
                                        if(db.setOwnStatusPast(vId) == 0 && db.insertUsedInventory(sId, vId, price) == 0) {
                                                System.out.printf("Please wait for Alset staff to take your " +
                                                                "vehicle id(%d) to service location id(%d). Your " +
                                                                "card will be credited $%.2f after your used vehicle " +
                                                                "is bought by another customer.",
                                                        vId, sId, (double) price);
                                        }
                                    } else if (yn.equals("No")) {
                                        System.out.println("Your sale is cancelled.");
                                    } else {
                                        System.out.println("Wrong input.");
                                    }
                                } else {
                                    System.out.println("Wrong input.");
                                }
                            }
                        }
                    } else {
                        System.out.println("Wrong input.");
                    }
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '9') {  // Buy Used
                while(true) {
                    int sId = getInt(in, "Please enter the service location id that you are at, or '0' for all service locations listing");
                    if (sId == 0) {
                        ArrayList<Database.ServiceLocation> sls = db.selectAllServiceLocations();
                        if (sls.size() == 0) {
                            System.out.println("Sorry, Alset has closed all of its service locations.");
                        } else {
                            for (int i = 0; i < sls.size(); i++) {
                                System.out.println(sls.get(i));
                            }
                        }
                        continue;
                    }
                    if (db.selectServiceLocation(sId) != null) {
                        ArrayList<Database.UsedInventory> uIs = db.selectUsedInventoriesIn(sId);
                        ArrayList<Integer> vIds = new ArrayList<>();
                        if(uIs.size() == 0){
                            System.out.printf("There is no used cars currently available at service location id(%d)", sId);
                        } else {
                            System.out.println("Select your desired used Alset vehicle from the following.");
                            for (int i = 0; i < uIs.size(); i++) {
                                vIds.add(uIs.get(i).vId);
                                Database.UsedVehicle v = db.selectUsedVehicleFromUsedInventory(uIs.get(i).vId, sId);
                                System.out.println("\n" + v);
                            }
                            int vId = getInt(in, "Enter vehicle id");
                            if(vIds.contains(vId)){
                                Database.UsedVehicle v = db.selectUsedVehicleFromUsedInventory(vId, sId);
                                System.out.println("Confirm your purchase of vehicle?\n" + v);
                                String yn = getString(in, "Enter 'Yes' or 'No'");
                                if(yn.equals("Yes")) {
                                    if (db.deleteUsedInventory(sId, vId) == 0 && db.insertOwn(id, vId, v.salePrice, "Current") == 0) {
                                        System.out.println("Congratulations. Enjoy your ride with your Alset vehicle.");
                                        break;
                                    }
                                } else if(!yn.equals("No")){
                                    System.out.println("Wrong input.");
                                    break;
                                }
                            }
                        }
                        break;
                    } else {
                        System.out.println("Wrong input.");
                        break;
                    }
                }
                System.out.println("Exiting to main menu...");
            } else if (action == '?') {
                menu();
            } else if (action == 'q') {
                System.out.println("Thank you for using Alset's Customer Interface.");
                break;
            } else {
                System.out.println("Wrong input.");
            }
        }
        db.disconnect();
    }
}
