/*
Project name: Alset Eccentric Vehicle Database
Author: Desai Xie, Lehigh University, Class of 2020
 */
package project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class EmployeeInterface {
    /**
     * Print the menu for our program
     */
    static void menu() {
        System.out.println("Enter the number twice for detailed description, once for choosing the option. ");
        System.out.println("  [1] Used Inventory");
        System.out.println("  [2] Recall");
        System.out.println("  [3] Show Room");
        System.out.println("  [4] New Delivery");
        System.out.println("  [5] Service Visit");
        System.out.println("  [6] Handel Service Visit");
        System.out.println("  [q] Quit Program");
        System.out.println("  [?] Help (this message)");
    }

    /**
     * Ask the user to enter a menu option; repeat until we get a valid option
     *
     * @param in A BufferedReader, for reading from the keyboard
     * @return The character corresponding to the chosen menu option
     */
    static char prompt(BufferedReader in) {
        // The valid actions:
        String actions = "123456q?";

        // We repeat until a valid single-character option is selected
        while (true) {
            System.out.println("\nPlease don't use this terminal if you are not an employee at Alset.");
            System.out.println("Your are at main menu");
            System.out.println("Enter an option:> ");
            String action;
            try {
                action = in.readLine();
            } catch (IOException e) {
                System.out.println("Couldn't access input from terminal");
                continue;
            }
            if (action.length() > 2) {
                System.out.println("Invalid Command");
                continue;
            }
            if(action.length() == 1){
                if (actions.contains(action)) {
                    return action.charAt(0);
                }
            }
            if(action.length() == 2){
                switch(action){
                    case "11":
                        System.out.println("Used Inventory\nShow all vehicles in the used inventory.");
                        break;
                    case "22":
                        System.out.println("Recall\nShow recalled Alset vehicles that are at this service location. Storage manager can remove them from database record after Alset headquarter come to collect them.");
                        break;
                    case "33":
                        System.out.println("Show Room\nCheck all vehicles in this service location.");
                        break;
                    case "44":
                        System.out.println("New Delivery\nShow all new vehicles ordered by customers that are to be picked up at this service location.");
                        break;
                    case "55":
                        System.out.println("Service Visit\nShow all finished service visit records at this service location.");
                        break;
                    case "66":
                        System.out.println("Handel Service Visit\nCheck the condition of the vehicle come in, and enter a description and estimate the cost to the record of this service visit.");
                        break;
                    case "qq":
                        System.out.println("Quit Program");
                        break;
                    case "??":
                        System.out.println("Help\nDisplay the options again.");
                        break;
                    default:
                        System.out.println("Invalid Command");
                        continue;
                }
                return prompt(in);
            }
        }
    }

    /**
     * Ask the user to enter a String message
     *
     * @param in      A BufferedReader, for reading from the keyboard
     * @param message A message to display when asking for input
     * @return The string that the user provided. May be "".
     */
    static String getString(BufferedReader in, String message) {
        String s;
        try {
            System.out.println(message + " :> ");
            s = in.readLine();
        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("Couldn't access stdin");
            return "";
        }
        return s;
    }

    /**
     * Ask the user to enter an integer
     *
     * @param in      A BufferedReader, for reading from the keyboard
     * @param message A message to display when asking for input
     * @return The integer that the user provided. On error, it will be -1
     */
    static int getInt(BufferedReader in, String message) {
        int i = -1;
        try {
            System.out.println(message + " :> ");
            i = Integer.parseInt(in.readLine());
        } catch (IOException e) {
            System.out.println("Couldn't access stdin");
        } catch (NumberFormatException e) {
            System.out.println("Didn't enter an integer");
        }
        return i;
    }



    public static void interFace() {
        // Get a fully-configured connection to the database, or exit
        // immediately
        Database db = Database.getDatabase();
        if (db == null)
            return;

        // Start our basic command-line interpreter:
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Welcome to Alset Eccentric Vehicle's Employee Interface.\n");
        int sId = -1;
        while(sId == -1) {
            sId = getInt(in, "Enter the service location id");
            if(sId < 1){
                System.out.println("This service location doesn't exist. Please try again.");
                sId = -1;
                continue;
            }
            if(db.selectServiceLocation(sId) == null){
                System.out.println("This service location doesn't exist. Please try again.");
                sId = -1;
                continue;
            }
        }
        int eId = -1;
        while(eId == -1) {
            eId = getInt(in, "Enter your employee id");
            if(eId < 1){
                System.out.println("This employee id doesn't exist. Please try again.");
                eId = -1;
            }
        }
        System.out.println("Login succeeds.");
        menu();
        while (true) {
            // Get the user's request
            char action = prompt(in);


            if (action == '1') {  // Used Inventory
                ArrayList<Database.UsedInventory> uIs = db.selectUsedInventoriesIn(sId);
                if(uIs.size() == 0){
                    System.out.println("There is no used cars currently available at this service location");
                } else {
                    System.out.println("Used inventory have the following vehicles stored.");
                    for (int i = 0; i < uIs.size(); i++) {
                        Database.UsedVehicle v = db.selectUsedVehicleFromUsedInventory(uIs.get(i).vId, sId);
                        System.out.println("\n" + v);
                    }
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '2') {  // Recall
                ArrayList<Integer> vIds = db.selectRecallsIn(sId);
                if(vIds.size() != 0){
                    System.out.println("This service location has the following recalled vehicles in storage.");
                    for (int i = 0; i < vIds.size(); i++) {
                        System.out.println("\n" + db.selectRecalledVehicle(vIds.get(i)));
                    }
                    System.out.println("Are they collected by Alset headquarter?");
                    String yn = getString(in, "Enter 'Yes' or 'No'");
                    if(yn.equals("Yes")){
                        if(db.deleteRecallsAt(sId) == 0) {
                            System.out.println("The above recalled vehicles are removed from record.");
                        }
                    } else if(!yn.equals("No")) {
                        System.out.println("Wrong input.");
                    }
                }else {
                    System.out.println("There is no recalled Alset vehicles currently at this service location.");
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '3') { // Show room
                ArrayList<Database.UsedVehicle> srs = db.selectShowroomAt(sId);
                if(srs.size() != 0) {
                    System.out.println("The show room at this service location has the following new vehicles.");
                    for (int i = 0; i < srs.size(); i++) {
                        System.out.println("\n" + srs.get(i));
                    }
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '4') {  // New delivery
                ArrayList<Database.UsedVehicle> nds = db.selectNewDeliveriesIn(sId);
                if (nds.size() != 0) {
                    for (int i = 0; i < nds.size(); i++) {
                        System.out.println("\n" + nds.get(i));
                    }
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '5') {
                ArrayList<Database.ServiceVisit> svs = db.selectFinishedServiceVisits(sId);
                if(svs.size() != 0) {
                    System.out.println("These are the finished service visit records at this service location.");
                    for (int i = 0; i < svs.size(); i++) {
                        System.out.println("\n" + svs.get(i));
                    }
                }
                System.out.println("Exiting to main menu...");


            } else if (action == '6') { // Handel Service Visit
                ArrayList<Database.ServiceVisit> svs = db.selectPartialServiceVisit(sId);
                ArrayList<Integer> repairIds = new ArrayList<>();
                if(svs.size() != 0) {
                    System.out.println("These are the unhandeled service visit records.");
                    for (int i = 0; i < svs.size(); i++) {
                        System.out.println("\n" + svs.get(i));
                        repairIds.add(svs.get(i).repairId);
                    }
                    System.out.println("Select one of them to handel: Give description, estimate cost");
                    int repairId = getInt(in, "Enter repair id");
                    if(repairIds.contains(repairId)){
                        String description = getString(in, "Enter description for this vehicle in this service visit");
                        int cost = getInt(in, "Enter cost for this service visit");
                       if(db.handelPartialServiceVisit(repairId, description, (double)cost) == 0){
                           System.out.println("This service visit record is updated");
                       }
                    } else {
                        System.out.println("Wrong input.");
                    }
                }
                System.out.println("Exiting to main menu...");



            } else if (action == '?') {
                menu();
            } else if (action == 'q') {
                System.out.println("Thank you for using Alset's Customer Interface.");
                break;
            } else {
                System.out.println("Wrong input.");
            }
        }
        db.disconnect();
    }
}
