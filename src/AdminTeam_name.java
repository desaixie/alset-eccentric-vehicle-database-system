import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.net.*;
import java.util.ArrayList;
import java.util.Map;

class Database {

    private Connection connection;

    private PreparedStatement createTableMessage = null; // Done
    private PreparedStatement dropTableMessage = null; // Done
    private PreparedStatement selectAllMessages = null; // Done
    private PreparedStatement selectOneMessage = null; // Done
    private PreparedStatement deleteOneMessage = null; // Done
    private PreparedStatement insertOneMessage = null; // Done
    private PreparedStatement editOneMessage = null; // Done
    private PreparedStatement deleteFile = null;  //Done

    private PreparedStatement createTableUser = null; // Done
    private PreparedStatement dropTableUser = null; // Done
    private PreparedStatement selectAllUsers = null; // Done
    private PreparedStatement selectOneUser = null; // Done
    private PreparedStatement deleteOneUser = null; // Done
    private PreparedStatement insertOneUser = null; // Done

    private PreparedStatement createTableComment = null; // Done
    private PreparedStatement dropTableComment = null; // Done
    private PreparedStatement insertOneComment = null;// Done
    private PreparedStatement editOneComment = null; // Done
    private PreparedStatement selectOneComment = null; //
    private PreparedStatement selectAllComments = null; //

    private PreparedStatement createTableUpVote = null; // Done
    private PreparedStatement dropTableUpVote = null; // Done
    private PreparedStatement createTableDownVote = null; // Done
    private PreparedStatement dropTableDownVote = null; // Done
    private PreparedStatement deleteUpVote = null; // Done
    private PreparedStatement deleteDownVote = null; // Done
    private PreparedStatement upVoteOne = null; // Done
    private PreparedStatement downVoteOne = null; // Done
    private PreparedStatement checkUpVote = null; // Done
    private PreparedStatement checkDownVote = null; // Done

    private PreparedStatement messageView= null;
    private PreparedStatement selectDetailedMessage = null;



    public static class Message {

        int message_id;
        int user_id;
        String subject;
        String text;
        int upVotes;
        int downVotes;
        String file_id;

        public Message(int pId, int pUser_id, String pSubject, String pText, int pUpVotes, int pDownVotes) {
            message_id = pId;
            user_id = pUser_id;
            subject = pSubject;
            text = pText;
            upVotes = pUpVotes;
            downVotes = pDownVotes;
        }

        @Override
        public String toString() {
            return "  [" + message_id + "] by user " + user_id + " with subject \"" + subject + "\", \"" + text + "\". Upvotes: "
                    + upVotes + ", Downvotes: " + downVotes + ", File ID (If Availible): " + file_id;
        }
    }

    public static class MessageDetail {

        int messageDetail_id;
        String username;
        String subject;
        String text;
        int upVotes;
        int downVotes;
        ArrayList<Comment> comments;

        public MessageDetail(int mId, String mUserName, String mSubject, String mText, int mUpVotes, int mDownVotes, ArrayList<Comment> mComments) {
            messageDetail_id = mId;
            username= mUserName;
            subject = mSubject;
            text = mText;
            upVotes = mUpVotes;
            downVotes = mDownVotes;
            comments = mComments;
        }

        @Override
        public String toString() {
            return "  [" + messageDetail_id + "] by user " + username + " with subject \"" + subject + "\", \"" + text + "\". Upvotes: "
                    + upVotes + ", Downvotes: " + downVotes + "comments: " + comments;
        }
    }

    public static class User {
        int user_id;
        String username;
        String realname;
        String email;

        public User(int uId, String uUserName, String uRealName, String uEmail) {
            user_id = uId;
            username = uUserName;
            realname = uRealName;
            email = uEmail;
        }

        @Override
        public String toString() {
            return "  [" + user_id + "] " + username + " " + realname + " " + email;
        }
    }

    public static class Comment {
        int id;
        int user_id;
        int message_id;
        String text;

        public Comment(int cId, int cUser_id, int cMessage_id, String cText) {
            id = cId;
            user_id = cUser_id;
            message_id = cMessage_id;
            text = cText;
        }

        @Override
        public String toString() {
            return "  [" + message_id + "] in message " + id + "by user " + user_id + ", \"" + text + "\"";
        }
    }

    public Database() {
    }

    public static Database getDatabase() {
        // Create an un-configured Database object
        Database db = new Database();

        Map<String, String> env = System.getenv();
        String db_url = env.get("DATABASE_URL");
        // Give the Database object a connection, fail if we cannot get one
        try {
            Class.forName("org.postgresql.Driver");
            URI dbUri = new URI(db_url);
            String username = dbUri.getUserInfo().split(":")[0];
            String password = dbUri.getUserInfo().split(":")[1];
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath()
                    + "?sslmode=require";
            Connection conn = DriverManager.getConnection(dbUrl, username, password);
            if (conn == null) {
                System.err.println("Error: DriverManager.getConnection() returned a null object");
                return null;
            }
            db.connection = conn;
        } catch (SQLException e) {
            System.err.println("Error: DriverManager.getConnection() threw a SQLException");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException cnfe) {
            System.out.println("Unable to find postgresql driver");
            return null;
        } catch (URISyntaxException s) {
            System.out.println("URI Syntax Error");
            return null;
        }

        // Attempt to create all of our prepared statements. If any of these
        // fail, the whole getDatabase() call should fail
        try {
            db.createTableMessage = db.connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS tblMessage (message_id SERIAL PRIMARY KEY, user_id INT REFERENCES tblUser (user_id), subject VARCHAR(50) "
                            + "NOT NULL, text VARCHAR(500) NOT NULL, upVotes SMALLINT, downVotes SMALLINT)");
            db.dropTableMessage = db.connection.prepareStatement("DROP TABLE tblMessage");

            // Standard CRUD operations
            db.deleteOneMessage = db.connection.prepareStatement("DELETE FROM tblMessage WHERE message_id = ?");
            db.insertOneMessage = db.connection
                    .prepareStatement("INSERT INTO tblMessage VALUES (default, ?, ?, ?, 0, 0)"); // check
            db.selectAllMessages = db.connection.prepareStatement("SELECT * FROM tblMessage");
            db.selectOneMessage = db.connection.prepareStatement("SELECT * from tblMessage WHERE message_id=?");
            db.editOneMessage = db.connection.prepareStatement("UPDATE tblMessage SET text = ? WHERE message_id = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }

        // Creating user Table
        try {

            db.createTableUser = db.connection.prepareStatement("CREATE TABLE IF NOT EXISTS " +
                    "tblUser (user_id SERIAL PRIMARY KEY, username VARCHAR(255), realname VARCHAR(255), email VARCHAR(255)) ");
            db.dropTableUser = db.connection.prepareStatement("DROP TABLE tblUser");
            db.selectAllUsers = db.connection.prepareStatement("SELECT * FROM tblUser");
            db.selectOneUser = db.connection.prepareStatement("SELECT * FROM tblUser WHERE user_id=?");
            db.deleteOneUser = db.connection.prepareStatement("DELETE FROM tblUser WHERE user_id = ?");
            db.insertOneUser = db.connection.prepareStatement("INSERT INTO tblUser VALUES (default, ?, ?, ?)"); // check
            //db.updatePassword = db.connection.prepareStatement("UPDATE tblUser SET hash = ?  where username = ?");
            //db.returnSalt = db.connection.prepareStatement("SELECT salt FROM tblUser WHERE username = ?");
            //db.returnHash = db.connection.prepareStatement("SELECT hash FROM tblUser WHERE username = ?");
        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }

        try {
            db.createTableComment = db.connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS tblComment (comment_id SERIAL PRIMARY KEY, user_id INT REFERENCES tblUser (user_id) , "
                            +" message_id INT REFERENCES tblMessage (message_id), comment_text VARCHAR(255))");
            db.dropTableComment = db.connection.prepareStatement("DROP TABLE tblComment");
            db.insertOneComment = db.connection.prepareStatement("INSERT INTO tblComment VALUES (default, ?,?,?)");
            db.editOneComment = db.connection.prepareStatement("UPDATE tblComment SET text = ? WHERE id = ?");
            db.selectAllComments = db.connection.prepareStatement("SELECT * FROM tblComment WHERE message_id = ?");
            db.selectOneComment = db.connection.prepareStatement("SELECT * FROM tblComment WHERE comment_id = ?");
        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }

        try {
            db.createTableUpVote = db.connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS tblUpVote ( user_id INT REFERENCES tblUser (user_id), message_id INT REFERENCES tblMessage (message_id))");
            db.dropTableUpVote = db.connection.prepareStatement("DROP TABLE tblUpVote");
            db.upVoteOne = db.connection.prepareStatement("UPDATE tblMessage SET upVotes = (upVotes+1) WHERE message_id = ?; "
                    + " INSERT INTO tblUpVote VALUES (?,?) ");
            db.checkUpVote = db.connection.prepareStatement(
                    "SELECT CAST( CASE WHEN EXISTS (SELECT * FROM tblUpVote WHERE user_id = ? and message_id = ?  ) THEN 1 ELSE 0 END AS BIT)");
            db.deleteUpVote = db.connection
                    .prepareStatement("UPDATE tblMessage SET upVotes = (upVotes-1) WHERE message_id = ?;"+
                            "DELETE FROM tblUpVote WHERE user_id = ? AND message_id = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }

        try {
            db.createTableDownVote = db.connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS tblDownVote (user_id INT REFERENCES tblUser (user_id) , message_id INT REFERENCES tblMessage (message_id))");
            db.dropTableDownVote = db.connection.prepareStatement("DROP TABLE tblDownVote");
            db.checkDownVote = db.connection
                    .prepareStatement("SELECT CAST( CASE WHEN EXISTS (SELECT * FROM tblDownVote WHERE user_id = ? and message_id = ? ) THEN 1 ELSE 0 END AS BIT)");
            db.downVoteOne = db.connection
                    .prepareStatement("UPDATE tblMessage SET downVotes = (downVotes+1) WHERE message_id = ?;"
                            + " INSERT INTO tblDownVote VALUES (?,?) ");
            db.deleteDownVote = db.connection
                    .prepareStatement("UPDATE tblMessage SET downVotes = (downVotes-1) WHERE message_id = ?;"+
                            "DELETE FROM tblDownVote WHERE user_id = ? AND message_id = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }

        try{

            db.messageView = db.connection.prepareStatement(
                    "CREATE VIEW vMessage AS SELECT message_id, m.text, m.subject, m.upVotes, m.downVotes , u.username, u.realname, u.email , c.comment_text, c.comment_id "
                            + "FROM tblMessage m, tblUser u , tblComment c WHERE m.user_id= u.user_id AND c.message_id = m.message_id"
            );
            db.selectDetailedMessage = db.connection.prepareStatement("select * from vMessage where message_id = ?");
        } catch(SQLException e){
            e.printStackTrace();

        }
        return db;
    }

    /**
     * Close the current connection to the database, if one exists.
     *
     * NB: The connection will always be null after this call, even if an error
     * occurred during the closing operation.
     *
     * @return True if the connection was cleanly closed, false otherwise
     */
    public boolean disconnect() {
        if (connection == null) {
            System.err.println("Unable to close connection: Connection was null");
            return false;
        }
        try {
            connection.close();
        } catch (SQLException e) {
            System.err.println("Error: Connection.close() threw a SQLException");
            e.printStackTrace();
            connection = null;
            return false;
        }
        connection = null;
        return true;
    }




    ////////////////////////////////////////////////////////////////////////////////////////////
    // Message table methods
    ////////////////////////////////////////////////////////////////////////////////////////////

    public void createTableMessage() {
        try {
            createTableMessage.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTableMessage() {
        try {
            dropTableMessage.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public MessageDetail selectDetailedMessage(int id) {
        MessageDetail res;
        ArrayList<Comment> resComms = new ArrayList<Comment>();
        try {
            selectDetailedMessage.setInt(1, id);
            ResultSet rs = selectDetailedMessage.executeQuery();
            while (rs.next()) {
                resComms.add( selectOneComment(rs.getInt("comment_id")));
            }
            res = new MessageDetail(rs.getInt("m.message_id"), rs.getString("u.username"), rs.getString("m.text"), rs.getString("m.subject"), rs.getInt("m.upVotes"), rs.getInt("m.downVotes"), resComms);
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int insertMessage(int user_id, String subject, String text) {
        int res = -1;
        try {
            insertOneMessage.setString(2, subject);
            insertOneMessage.setString(3, text);
            insertOneMessage.setInt(1, user_id);
            res = insertOneMessage.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<Message> selectAllMessages() {
        ArrayList<Message> res = new ArrayList<Message>();
        try {
            ResultSet rs = selectAllMessages.executeQuery();
            while (rs.next()) {
                res.add(new Message(rs.getInt("message_id"), rs.getInt("user_id"), rs.getString("subject"),
                        rs.getString("text"), rs.getInt("upVotes"), rs.getInt("downVotes")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Message selectOneMessage(int id) {
        Message res = null;
        try {
            selectOneMessage.setInt(1, id);
            ResultSet rs = selectOneMessage.executeQuery();
            if (rs.next()) {
                res = new Message(rs.getInt("message_id"), rs.getInt("user_id"), rs.getString("subject"),
                        rs.getString("text"), rs.getInt("upVotes"), rs.getInt("downVotes"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int deleteMessage(int id) {
        int res = -1;
        try {
            deleteOneMessage.setInt(1, id);
            res = deleteOneMessage.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public Message editOneMessage(int id, String text) {
        int res = -1;
        try {
            editOneMessage.setString(1, text);
            editOneMessage.setInt(2, id);
            res = editOneMessage.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (res != -1) {
            return selectOneMessage(id);
        } else {
            return null;
        }

    }



    ///////////////////////////////////////////////////////////////////////////////////////////
    // Upvote & downvote
    ///////////////////////////////////////////////////////////////////////////////////////////

    public void createTableUpVote() {
        try {
            createTableUpVote.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTableUpVote() {
        try {
            dropTableUpVote.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // DownVote
    public void createTableDownVote() {
        try {
            createTableDownVote.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTableDownVote() {
        try {
            dropTableDownVote.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /*
     *
     * @param user_id
     *
     * @param message_id
     *
     * @return message if succesfull, return null if upvote or downvote already
     * existed
     **
     *
     */
    public Message upVote(int user_id, int message_id) {
        int res = -1;
        try {
            checkUpVote.setInt(1, user_id);
            checkUpVote.setInt(2, message_id);
            ResultSet setUp = checkUpVote.executeQuery();

            checkDownVote.setInt(1, user_id);
            checkDownVote.setInt(2, message_id);
            ResultSet setDown = checkDownVote.executeQuery();
            if (!setUp.next()) {
                if (setDown.next()){
                    deleteDownVote.setInt(1,message_id);
                    deleteDownVote.setInt(2, user_id);
                    deleteDownVote.setInt(3, message_id);
                    deleteDownVote.executeUpdate();
                }
                upVoteOne.setInt(1, user_id);
                upVoteOne.setInt(2, message_id);
                res = upVoteOne.executeUpdate();
            } else {
                // if upvote exists function returns -1
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (res != -1) {
            return selectOneMessage(message_id);
        } else {
            return null;
        }

    }

    public Message downVote(int user_id, int message_id) {
        int res = -1;
        try {
            checkUpVote.setInt(1, user_id);
            checkUpVote.setInt(2, message_id);
            ResultSet setUp = checkUpVote.executeQuery();

            checkDownVote.setInt(1, user_id);
            checkDownVote.setInt(2, message_id);
            ResultSet setDown = checkDownVote.executeQuery();
            if (!setDown.next()) {
                if (setUp.next()) {
                    deleteUpVote.setInt(1,message_id);
                    deleteUpVote.setInt(2, user_id);
                    deleteUpVote.setInt(3, message_id);
                    deleteUpVote.executeUpdate();
                }
                downVoteOne.setInt(1, user_id);
                downVoteOne.setInt(2, message_id);
                res = downVoteOne.executeUpdate();
            } else {

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (res != -1) {
            return selectOneMessage(message_id);
        } else {
            return null;
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // User table methods
    ////////////////////////////////////////////////////////////////////////////////////////////


    public void createTableUser() {
        try {
            createTableUser.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTableUser() {
        try {
            dropTableUser.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User selectOneUser(int id) {
        User res = null;
        try {
            selectOneUser.setInt(1, id);
            ResultSet rs = selectOneUser.executeQuery();
            if (rs.next()) {
                res = new User(rs.getInt("user_id"), rs.getString("userName"), rs.getString("realName"),
                        rs.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<User> selectAllUsers() {
        ArrayList<User> res = new ArrayList<User>();
        try {
            ResultSet rs = selectAllUsers.executeQuery();
            while (rs.next()) {
                res.add(new User(rs.getInt("user_id"), rs.getString("username"), rs.getString("realname"),
                        rs.getString("email")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int deleteUser(int id) {
        int res = -1;
        try {
            deleteOneUser.setInt(1, id);
            res = deleteOneUser.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }


    public int insertUser(String username, String realName, String email) {
        int count = 0;
        try {
            insertOneUser.setString(1, username);
            insertOneUser.setString(2, realName);
            insertOneUser.setString(3, email);
            count += insertOneUser.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }



    /////////////////////////////////////////////////////////////////////////////////////////////
    // Comment table methods
    /////////////////////////////////////////////////////////////////////////////////////////////
    public void createTableComment() {
        try {
            createTableComment.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTableComment() {
        try {
            dropTableComment.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int insertComment(int user_id, int message_id, String text) {
        int res = -1;
        try {
            insertOneComment.setInt(1, user_id);
            insertOneComment.setInt(2, message_id);
            insertOneComment.setString(3, text);

            res = insertOneComment.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public Comment editOneComment(int id, String text) {
        int res = -1;
        try {
            editOneComment.setString(1, text);
            editOneComment.setInt(2, id);
            res = editOneComment.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (res != -1) {
            return selectOneComment(id);
        } else {
            return null;
        }

    }

    public Comment selectOneComment(int id) {
        Comment res = null;
        try {
            selectOneComment.setInt(1, id);
            ResultSet rs = selectOneComment.executeQuery();
            if (rs.next()) {
                res = new Comment(rs.getInt("comment_id"), rs.getInt("user_id"), rs.getInt("message_id"),
                        rs.getString("comment_text"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<Comment> selectAllCommentsofMessage(int message_id) {
        ArrayList<Comment> res = new ArrayList<Comment>();
        try {
            selectAllComments.setInt(1,message_id);
            ResultSet rs = selectAllComments.executeQuery();
            while (rs.next()) {
                res.add(new Comment(rs.getInt("comment_id"), rs.getInt("user_id"), rs.getInt("message_id"),
                        rs.getString("comment_text")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


}

