/*
Project name: Alset Eccentric Vehicle Database
Author: Desai Xie, Lehigh University, Class of 2020
 */
package project;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;


public class Database {
    public static class Customer {
        public int id;
        public String name;
        public String email;

        public Customer(int id, String name, String email) {
            this.id = id;
            this.name = name;
            this.email = email;
        }
    }

    public static class HasCard {
        public int id;
        public long cNumber;
        public String cType;

        public HasCard(int id, long cNumber, String cType) {
            this.id = id;
            this.cNumber = cNumber;
            this.cType = cType;
        }

        @Override
        public String toString() {
            int first = (int)(cNumber / 1000000000000L);
            int second = (int)((cNumber - first * 1000000000000L) / 100000000L);
            int third = (int)((cNumber - first * 1000000000000L - second * 100000000L) / 10000L);
            int fourth = (int)(cNumber - first * 1000000000000L - second * 100000000L - third * 10000L);
            return "Card number: " + first + " " + second + " " + third + " " + fourth + ". Card type: " + cType;
        }
    }

    public static class LivesIn {
        public int id;
        public String planet;
        public String country;
        public String city;
        public String street;

        public LivesIn(int id, String planet, String country, String city, String street) {
            this.id = id;
            this.planet = planet;
            this.country = country;
            this.city = city;
            this.street = street;
        }
    }

    public static class Address {
        public String planet;
        public String country;
        public String city;
        public String street;

        public Address(String planet, String country, String city, String street) {
            this.planet = planet;
            this.country = country;
            this.city = city;
            this.street = street;
        }

        @Override
        public String toString() {
            return street + "\n\t" + city + ", " + country + ", " + planet;
        }
    }

    public static class VType {
        public String typeId;
        public String model;
        public int year;
        public String options;
        public double salePrice;

        public VType(String typeId, String model, int year, String options, double salePrice) {
            this.typeId = typeId;
            this.model = model;
            this.year = year;
            this.options = options;
            this.salePrice = salePrice;
        }

        @Override
        public String toString() {
            return String.format("Vehicle type: %-13s  Model: %-1s  Year: %-4s  Options: %-8s  Sale price: $%-6.2f",
                    typeId, model, year, options, salePrice);
        }
    }

    public static class Vehicle {
        public int vId;
        public String typeId;
        public String needMaintain;
        public String saleHistory;

        public Vehicle(int vId, String typeId, String needMaintain, String saleHistory) {
            this.vId = vId;
            this.typeId = typeId;
            this.needMaintain = needMaintain;
            this.saleHistory = saleHistory;
        }

        @Override
        public String toString() {
            return String.format("Vehicle id: %-6d  Type id: %-13s  Need maintain: %s  Sale History: %-4s",
                    vId, typeId, needMaintain, saleHistory);
        }
    }

    public static class Own {
        public int id;
        public int vId;
        public double boughtPrice;
        public String status;

        public Own(int id, int vId, double boughtPrice, String status) {
            this.id = id;
            this.vId = vId;
            this.boughtPrice = boughtPrice;
            this.status = status;
        }
    }

    public static class OwnedVehicle {
        public int id;
        public int vId;
        public String typeId;
        public String needMaintain;
        public double boughtPrice;
        public String model;
        public int year;
        public String options;
        public double salePrice;

        public OwnedVehicle(int id, int vId, String typeId, String needMaintain, double boughtPrice, String model, int year, String options, double salePrice) {
            this.id = id;
            this.vId = vId;
            this.typeId = typeId;
            this.needMaintain = needMaintain;
            this.boughtPrice = boughtPrice;
            this.model = model;
            this.year = year;
            this.options = options;
            this.salePrice = salePrice;
        }

        @Override
        public String toString() {
            return String.format("Vehicle id: %-6d  Type id: %-13s  Need maintain: %s  Bought Price: $%-6.2f  " +
                    "\nModel: %s  Year: %d  Options: %-8s  Sale Price: $%-6.2f", vId, typeId, needMaintain,
                    boughtPrice, model, year, options, salePrice);
        }
    }

    public static class UsedVehicle {
        public int vId;
        public String typeId;
        public String needMaintain;
        public String model;
        public int year;
        public String options;
        public double salePrice;

        public UsedVehicle(int vId, String typeId, String needMaintain, String model, int year, String options, double salePrice) {
            this.vId = vId;
            this.typeId = typeId;
            this.needMaintain = needMaintain;
            this.model = model;
            this.year = year;
            this.options = options;
            this.salePrice = salePrice;
        }

        @Override
        public String toString() {
            if(needMaintain.equals("Showroom") || needMaintain.equals("NewDelivery")) {
                return String.format("Vehicle id: %-6d  Type id: %-13s  Sale History: New\nModel: %s  Year: %d  " +
                                "Options: %-8s  Sale Price: $%-6.2f", vId, typeId, model, year, options, salePrice);
            } else if(salePrice == -1.00){
                return String.format("Vehicle id: %-6d  Type id: %-13s  Need maintain: %s\nModel: %s  Year: %d  " +
                                "Options: %-8s", vId, typeId, needMaintain,
                        model, year, options);
            }
            else {
                return String.format("Vehicle id: %-6d  Type id: %-13s  Need maintain: %s\nModel: %s  Year: %d  " +
                                "Options: %-8s  Sale Price: $%-6.2f", vId, typeId, needMaintain,
                        model, year, options, salePrice);
            }
        }
    }

    public static class ServiceLocation {
        public int sId;
        public String handelModels;
        public String planet;
        public String country;
        public String city;
        public String street;

        public ServiceLocation(int sId, String handelModels, String planet, String country, String city, String street) {
            this.sId = sId;
            this.handelModels = handelModels;
            this.planet = planet;
            this.country = country;
            this.city = city;
            this.street = street;
        }

        @Override
        public String toString() {
            return "Service Location id: " + sId + "\nModels Handeled: " + handelModels + "\nAddress: "
                    + new Address(planet,country,city,street).toString();
        }
    }

    public static class UsedInventory {
        public int sId;
        public int vId;
        public double salePrice;

        public UsedInventory(int sId, int vId, double salePrice) {
            this.sId = sId;
            this.vId = vId;
            this.salePrice = salePrice;
        }
    }

    public static class ServiceVisit {
        public int repairId;
        public int id;
        public int vId;
        public int sId;
        public String description;
        public String repairDate;
        public double cost;

        public ServiceVisit(int repairId, int id, int vId, int sId, String description, String repairDate, double cost) {
            this.repairId = repairId;
            this.id = id;
            this.vId = vId;
            this.sId = sId;
            this.description = description;
            this.repairDate = repairDate;
            this.cost = cost;
        }

        @Override
        public String toString() {
            return String.format("Repair id: %-6d  Customer id: %-6d  Vehicle Id: %-6d  Service location id: %-6d  " +
                    "\nDescription: %s\nRepair Date: %s  Cost: %-7.2f", repairId, id, vId, sId, description, repairDate, cost);
        }
    }

    private Connection conn;

    private PreparedStatement insertCustomer = null;
    private PreparedStatement selectCustomer = null;
    private PreparedStatement editCustomer = null;
    private PreparedStatement deleteCustomer = null;
    private PreparedStatement selectAllCustomerIds = null;

    private PreparedStatement insertHasCard = null;
    private PreparedStatement selectHasCards = null;
    private PreparedStatement editHasCard = null;
    private PreparedStatement deleteHasCard = null;

    private PreparedStatement insertLivesIn = null;
    private PreparedStatement selectLivesIn = null;
    private PreparedStatement editLivesIn = null;
    private PreparedStatement deleteLivesIn = null;

    private PreparedStatement insertVehicle = null;
    private PreparedStatement selectVehicle = null;
    private PreparedStatement editVehicle = null;
    private PreparedStatement deleteVehicle = null;
    private PreparedStatement selectAllVehicleIds = null;
    private PreparedStatement selectNewVehicleWith = null;
    private PreparedStatement setSaleHistoryUsed = null;

    private PreparedStatement insertOwn = null;
    private PreparedStatement selectOwn = null;
    private PreparedStatement selectOwnsWithId = null;
    private PreparedStatement editOwn = null;
    private PreparedStatement deleteOwn = null;
    private PreparedStatement setOwnStatusPast = null;

    private PreparedStatement insertVType = null;
    private PreparedStatement selectVType = null;
    private PreparedStatement selectAllVTypes = null;
    private PreparedStatement selectVTypesWith = null;
    private PreparedStatement editVType = null;
    private PreparedStatement deleteVType = null;

    private PreparedStatement insertServiceLocation = null;
    private PreparedStatement selectServiceLocation = null;
    private PreparedStatement editServiceLocation = null;
    private PreparedStatement deleteServiceLocation = null;
    private PreparedStatement selectAllServiceLocations = null;
    private PreparedStatement selectServiceLocationsThatHandel = null;

    private PreparedStatement insertUsedInventory = null;
    private PreparedStatement selectUsedInventoriesIn = null;
    private PreparedStatement editUsedInventory = null;
    private PreparedStatement deleteUsedInventory = null;

    private PreparedStatement insertRecall = null;
    private PreparedStatement selectRecallsIn = null;
    private PreparedStatement editRecall = null;
    private PreparedStatement deleteRecall = null;
    private PreparedStatement deleteRecallsAt = null;

    private PreparedStatement insertShowroom = null;
    private PreparedStatement selectShowroomAt = null;
    private PreparedStatement editShowroom = null;
    private PreparedStatement deleteShowroom = null;

    private PreparedStatement insertNewDelivery = null;
    private PreparedStatement selectNewDeliveriesIn = null;
    private PreparedStatement editNewDelivery = null;
    private PreparedStatement deleteNewDelivery = null;
    private PreparedStatement selectNewDeliveredVId = null;

    private PreparedStatement insertServiceVisit = null;
    private PreparedStatement insertPartialServiceVisit = null;
    private PreparedStatement selectPartialServiceVisit = null;
    private PreparedStatement selectServiceVisit = null;
    private PreparedStatement editServiceVisit = null;
    private PreparedStatement deleteServiceVisit = null;
    private PreparedStatement handelPartialServiceVisit = null;
    private PreparedStatement selectFinishedServiceVisitsIn = null;

    private PreparedStatement getVIdWithTypeId = null;
    private PreparedStatement getSIdWithModel = null;

    public Database(){
    }

    public static Database getDatabase(){
        Database db = new Database();

        Scanner in = new Scanner(System.in);
        System.out.println("Enter Oracle user id:");
        String userid = in.next();
        System.out.println("Enter Oracle password:");
        String password = in.next();
        //catch any possible exception when logging in
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            db.conn = DriverManager.getConnection("jdbc:oracle:thin:@edgar0.cse.lehigh.edu:1521:cse241", userid, password);
            if(db.conn == null){
                System.err.println("Error: DriverManager.getConnection returned a null object");
                return null;
            }
        } catch(ClassNotFoundException | SQLException ex){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            ex.printStackTrace();
            return null;
        }


        try{
            db.insertCustomer = db.conn.prepareStatement("insert into customer (name, email) values (?, ?)", new int[] {1});
            db.selectCustomer = db.conn.prepareStatement("select * from customer where id = ?");
            db.editCustomer = db.conn.prepareStatement("update customer set name = ?, email = ? where id = ?");

            db.insertHasCard = db.conn.prepareStatement("insert into has_card values (?, ?, ?)");
            db.selectHasCards = db.conn.prepareStatement("select * from has_card where id = ?");
            db.deleteHasCard = db.conn.prepareStatement("delete has_card where id = ? and c_number = ?");

            db.insertLivesIn = db.conn.prepareStatement("insert into lives_in values (?, ?, ?, ?, ?)");
            db.editLivesIn = db.conn.prepareStatement("update lives_in set planet = ?, country = ?, city = ?, street = ? where id = ?");
            db.selectLivesIn = db.conn.prepareStatement("select * from lives_in where id = ?");

            db.insertVehicle = db.conn.prepareStatement("insert into vehicle (type_id, need_maintain, sale_history) values (?, ?, ?)", new int[] {1});
            db.selectVehicle = db.conn.prepareStatement("select * from vehicle where v_id = ?");
            db.selectNewVehicleWith = db.conn.prepareStatement("select * from vehicle where sale_history = 'New' and type_id = ?");
            db.setSaleHistoryUsed = db.conn.prepareStatement("update vehicle set sale_history = 'Used' where v_id = ?");

            db.insertOwn = db.conn.prepareStatement("insert into own values (?, ?, ?, ?)");
            db.selectOwn = db.conn.prepareStatement("select * from own where id = ? and v_id = ? and status = ?");
            db.selectOwnsWithId = db.conn.prepareStatement("select * from own where id = ?");
            db.deleteOwn = db.conn.prepareStatement("delete own where id = ? and v_id = ? and status = ?");
            db.setOwnStatusPast = db.conn.prepareStatement("update own set status = 'Past' where v_id = ?");

            db.insertVType = db.conn.prepareStatement("insert into v_type values (?, ?, ?, ?, ?)");
            db.selectVType = db.conn.prepareStatement("select * from v_type where type_id = ?");
            db.selectAllVTypes = db.conn.prepareStatement("select * from v_type");
            db.selectVTypesWith = db.conn.prepareStatement("select * from v_type where model = ?");

            db.insertServiceLocation = db.conn.prepareStatement("insert into service_location(handel_models, planet, country, city, street) values (?, ?, ?, ?, ?)", new int[] {1});
            db.selectServiceLocation = db.conn.prepareStatement("select * from service_location where s_id = ?");
            db.selectAllServiceLocations = db.conn.prepareStatement("select * from service_location");
            db.selectServiceLocationsThatHandel = db.conn.prepareStatement("select * from service_location where handel_models like ?");

            db.insertUsedInventory = db.conn.prepareStatement("insert into used_inventory values (?, ?, ?)");
            db.selectUsedInventoriesIn = db.conn.prepareStatement("select * from used_inventory where s_id = ?");
            db.deleteUsedInventory = db.conn.prepareStatement("delete used_inventory where s_id = ? and v_id = ?");

            db.insertRecall = db.conn.prepareStatement("insert into recall values (?, ?)");
            db.selectRecallsIn = db.conn.prepareStatement("select * from recall where s_id = ?");
            db.deleteRecallsAt = db.conn.prepareStatement("delete recall where s_id = ?");

            db.insertShowroom = db.conn.prepareStatement("insert into showroom values (?, ?)");
            db.selectShowroomAt = db.conn.prepareStatement("select * from showroom where s_id = ?");

            db.insertNewDelivery = db.conn.prepareStatement("insert into new_delivery values (?, ?, ?)");
            db.selectNewDeliveredVId = db.conn.prepareStatement("select v_Id from new_delivery where s_id = ? and id = ?");
            db.selectNewDeliveriesIn = db.conn.prepareStatement("select * from new_delivery where s_id = ?");
            db.deleteNewDelivery = db.conn.prepareStatement("delete new_delivery where s_id = ? and v_id = ? and id = ?");

            db.insertServiceVisit = db.conn.prepareStatement("insert into service_visit(id, v_id, s_id, description, repair_date, cost) values (?, ?, ?, ?, ?, ?)", new int[] {1});
            db.insertPartialServiceVisit = db.conn.prepareStatement("insert into service_visit(id, v_id, s_id, repair_date) values (?, ?, ?, ?)", new int[] {1});
            db.selectPartialServiceVisit = db.conn.prepareStatement("select * from service_visit where description is null and cost is null and s_id = ?");
            db.handelPartialServiceVisit = db.conn.prepareStatement("update service_visit set description = ?, cost = ? where repair_id = ?");
            db.selectFinishedServiceVisitsIn = db.conn.prepareStatement("select * from service_visit where s_id = ? and description is not null and cost is not null");

            db.getVIdWithTypeId = db.conn.prepareStatement("select v_id from vehicle where type_id = ?");
//            db.getSIdWithModel = db.conn.prepareStatement("select s_id from service_location where handel_models")

        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            System.out.println("Error creating prepared statements");
            e.printStackTrace();
            db.disconnect();
            return null;
        }



        return db;
    }

    public boolean disconnect() {
        if (conn == null) {
            System.err.println("Unable to close connection: Connection was null");
            return false;
        }
        try {
            conn.close();
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            System.err.println("Error: Connection.close() threw a SQLException");
            e.printStackTrace();
            conn = null;
            return false;
        }
        conn = null;
        return true;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // Library methods that uses PreparedStatements
    // Execpt for "select" methods and insertCustomer, insertVehicle, insertServiceLocation, insertServiceViist,
    // all the other methods returns 0 on success or -1 on failure


    // Consider return newId for those with default id
    public int insertCustomer(String name, String email){
        int res = -1;
        try {
            insertCustomer.setString(1, name);
            insertCustomer.setString(2, email);
            insertCustomer.executeUpdate();
            ResultSet rs = insertCustomer.getGeneratedKeys();
            rs.next();
            res = rs.getInt(1);
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public Customer selectOneCustomer(int id){
        Customer res = null;
        try {
            selectCustomer.setInt(1, id);
            ResultSet rs = selectCustomer.executeQuery();
            if(rs.next()){
                res = new Customer(rs.getInt(1), rs.getString(2), rs.getString(3));
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    // Provide null if not editing that column
    public int editCustomer(int id, String name, String email){

        // select the customer
        Customer res = null;
        try {
            selectCustomer.setInt(1, id);
            ResultSet rs = selectCustomer.executeQuery();
            if(rs.next()){
                res = new Customer(rs.getInt(1), rs.getString(2), rs.getString(3));
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            System.err.println("Trying to edit nonexisting row.");
            return -1;
        }
        if(res == null){
            System.err.println("Trying to edit nonexisting row.");
            return -1;
        }
        if(name == null){
            name = res.name;
        }
        if(email == null){
            email = res.email;
        }
        try {
            editCustomer.setString(1, name);
            editCustomer.setString(2, email);
            editCustomer.setInt(3, id);
            editCustomer.executeUpdate();
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
        return 0;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertHasCard(int id, long cNumber, String cType){
        try {
            insertHasCard.setInt(1, id);
            insertHasCard.setLong(2, cNumber);
            insertHasCard.setString(3,cType);
            insertHasCard.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public ArrayList<HasCard> selectCards(int id){
        ArrayList<HasCard> res = new ArrayList<>();
        try {
            selectHasCards.setInt(1, id);
            ResultSet rs = selectHasCards.executeQuery();
            while(rs.next()){
                res.add(new HasCard(rs.getInt(1), rs.getLong(2), rs.getString(3)));
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public int deleteHasCard(int id, long cNumber){
        try{
            deleteHasCard.setInt(1, id);
            deleteHasCard.setLong(1, cNumber);
            deleteHasCard.executeUpdate();
            return 0;
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertLivesIn(int id, String planet, String country, String city, String street){
        try {
            insertLivesIn.setInt(1, id);
            insertLivesIn.setString(2, planet);
            insertLivesIn.setString(3, country);
            insertLivesIn.setString(4, city);
            insertLivesIn.setString(5, street);
            insertLivesIn.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public Address selectLivesIn(int id){
        Address res = null;
        try {
            selectLivesIn.setInt(1, id);
            ResultSet rs = selectLivesIn.executeQuery();
            if(rs.next()){
                res = new Address(rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }

        return res;
    }

    public int editLivesIn(int id, String planet, String country, String city, String street){
        try {
            editLivesIn.setString(1, planet);
            editLivesIn.setString(2, country);
            editLivesIn.setString(3, city);
            editLivesIn.setString(4, street);
            editLivesIn.setInt(5, id);
            editLivesIn.executeUpdate();
            return 0;
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return -1;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertVehicle(String typeId, String needMaintain, String saleHistory){
        int res = -1;
        try {
            insertVehicle.setString(1, typeId);
            insertVehicle.setString(2, needMaintain);
            insertVehicle.setString(3, saleHistory);
            insertVehicle.executeUpdate();
            ResultSet rs = insertVehicle.getGeneratedKeys();
            rs.next();
            res = rs.getInt(1);
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public Vehicle selectVehicle(int vId){
        Vehicle res = null;
        try {
            selectVehicle.setInt(1, vId);
            ResultSet rs = selectVehicle.executeQuery();
            if(rs.next()){
                res = new Vehicle(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }

        return res;
    }

    public ArrayList<OwnedVehicle> selectOwnedVehicles(int id){
        ArrayList<OwnedVehicle> res = new ArrayList<>();
        try {
            selectOwnsWithId.setInt(1, id);
            ResultSet rs = selectOwnsWithId.executeQuery();
            while(rs.next()){
                if(rs.getString(4).equals("Current")){  // only select current ownership
                    int vId = rs.getInt(2);
                    double boughtPrice = rs.getInt(3);

                    selectVehicle.setInt(1, vId);
                    ResultSet rs2 = selectVehicle.executeQuery();
                    if(rs2.next()){
                        String typeId = rs2.getString(2);
                        String needMaintain = rs2.getString(3);
                        selectVType.setString(1, typeId);
                        ResultSet rs3 = selectVType.executeQuery();
                        if(rs3.next()) {
                            String model = rs3.getString(2);
                            int year = rs3.getInt(3);
                            String options = rs3.getString(4);
                            double salePrice = rs3.getDouble(5);
                            res.add(new OwnedVehicle(id, vId, typeId, needMaintain, boughtPrice, model, year, options, salePrice));
                        }
                    }
                }
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public UsedVehicle selectUsedVehicleFromUsedInventory(int vId, int sId){
        UsedVehicle res = null;
        Vehicle v = selectVehicle(vId);
        if(v != null) {
            VType vt = selectVType(v.typeId);
            if(vt != null){
                ArrayList<UsedInventory> uis = selectUsedInventoriesIn(sId);
                if(uis.size() != 0){
                    for (int i = 0; i < uis.size(); i++) {
                        if(uis.get(i).vId == vId){
                            double salePrice = uis.get(i).salePrice;
                            res = new UsedVehicle(v.vId, v.typeId, v.needMaintain, vt.model, vt.year, vt.options, salePrice);
                        }
                    }
                }
            }
        }
        return res;
    }

    public Vehicle selectNewVehicleWith(String typeId){
        Vehicle res = null;
        try {
            selectNewVehicleWith.setString(1, typeId);
            ResultSet rs = selectNewVehicleWith.executeQuery();
            if(rs.next()){
                res = new Vehicle(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    // Will remain 'New' until the user picks it up at the specified serviceLocation
    public int buyNewVehicle(int id, int vId, double boughtPrice, int sId){
        int a = insertOwn(id, vId, boughtPrice, "Current");
        int b = insertNewDelivery(sId, vId, id);
        if(a == 0 && b == 0) {
            return 0;
        } else { // One of a or b or both is -1
            if(a == 0){
                // Undo insertOwn
                deleteOwn(id, vId, "Current");
            }
            if (b == 0){
                // Undo insertNewDelivery
                deleteNewDelivery(sId, vId, id);
            }
            // If both are -1, then no undo process is needed
            return -1;
        }
    }

    public int setSaleHistoryUsed(int vId){
        try {
            setSaleHistoryUsed.setInt(1, vId);
            setSaleHistoryUsed.executeUpdate();
            return 0;
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertOwn(int id, int vId, double boughtPrice, String status){
        try {
            insertOwn.setInt(1, id);
            insertOwn.setInt(2, vId);
            insertOwn.setDouble(3, boughtPrice);
            insertOwn.setString(4, status);
            insertOwn.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public Own selectOwn(int id, int vId, String status){
        Own res = null;
        try {
            selectOwn.setInt(1, id);
            selectOwn.setInt(2, vId);
            selectOwn.setString(3, status);
            ResultSet rs = selectOwn.executeQuery();
            if(rs.next()){
                res = new Own(rs.getInt(1), rs.getInt(2), rs.getDouble(3), rs.getString(4));
            }
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public int deleteOwn(int id, int vId, String status){
        try {
            deleteOwn.setInt(1, id);
            deleteOwn.setInt(2, vId);
            deleteOwn.setString(3, status);
            deleteOwn.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    // No need for customer id since simply set all own records of the vehicle to past is fine
    int setOwnStatusPast(int vId){
        try {
            setOwnStatusPast.setInt(1, vId);
            setOwnStatusPast.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertVType(String model, int year, String options, double salePrice){
        try {
            String typeId = model + year + options;
            insertVType.setString(1, typeId);
            insertVType.setString(2, model);
            insertVType.setInt(3, year);
            insertVType.setString(4, options);
            insertVType.setDouble(5, salePrice);
            insertVType.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }
    public int insertVType(String typeId, double salePrice){
        try {
            String model = typeId.substring(0,1);
            int year = Integer.parseInt(typeId.substring(1,5));
            String options = typeId.substring(5, typeId.length());
            insertVType.setString(1, typeId);
            insertVType.setString(2, model);
            insertVType.setInt(3, year);
            insertVType.setString(4, options);
            insertVType.setDouble(5, salePrice);
            insertVType.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }


    public VType selectVType(String typeId){
        VType res = null;
        try {
            selectVType.setString(1, typeId);
            ResultSet rs = selectVType.executeQuery();
            if(rs.next()){
                res = new VType(typeId, rs.getString(2), rs.getInt(3), rs.getString(4), rs.getDouble(5));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }

        return res;
    }


    public ArrayList<VType> selectVTypesWith(String model){
        ArrayList<VType> res = new ArrayList<>();
        try{
            selectVTypesWith.setString(1, model);
            ResultSet rs = selectVTypesWith.executeQuery();
            while(rs.next()){
                res.add(new VType(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getDouble(5)));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<VType> selectAllVTypes(){
        ArrayList<VType> res = new ArrayList<>();
        try {
            ResultSet rs = selectAllVTypes.executeQuery();
            while(rs.next()){
                res.add(new VType(rs.getString(1), rs.getString(2), rs.getInt(3),
                        rs.getString(4), rs.getDouble(5)));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertServiceLocation(String handelModels, String planet, String country, String city, String street){
        int res = -1;
        try {
            insertServiceLocation.setString(1, handelModels);
            insertServiceLocation.setString(2, planet);
            insertServiceLocation.setString(3, country);
            insertServiceLocation.setString(4, city);
            insertServiceLocation.setString(5, street);
            insertServiceLocation.executeUpdate();
            ResultSet rs = insertServiceLocation.getGeneratedKeys();
            if(rs.next()) {
                res = rs.getInt(1);
            }
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public ServiceLocation selectServiceLocation(int sId){
        ServiceLocation res = null;
        try {
            selectServiceLocation.setInt(1,sId);
            ResultSet rs = selectServiceLocation.executeQuery();
            if(rs.next()){
                res = new ServiceLocation(sId, rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<ServiceLocation> selectAllServiceLocations(){
        ArrayList<ServiceLocation> res = new ArrayList<>();
        try {
            ResultSet rs = selectAllServiceLocations.executeQuery();
            while(rs.next()){
                res.add(new ServiceLocation(rs.getInt(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getString(6)));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<ServiceLocation> selectServiceLocationsThatHandel(String model){
        ArrayList<ServiceLocation> res = new ArrayList<>();
        try {
            selectServiceLocationsThatHandel.setString(1, "%" + model + "%");
            ResultSet rs = selectServiceLocationsThatHandel.executeQuery();
            while(rs.next()){
                res.add(new ServiceLocation(rs.getInt(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getString(6)));
            }
        } catch (SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertUsedInventory(int sId, int vId, double salePrice){
        try {
            insertUsedInventory.setInt(1, sId);
            insertUsedInventory.setInt(2, vId);
            insertUsedInventory.setDouble(3, salePrice);
            insertUsedInventory.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public ArrayList<UsedInventory> selectUsedInventoriesIn(int sId){
        ArrayList<UsedInventory> res = new ArrayList<>();
        try {
            selectUsedInventoriesIn.setInt(1, sId);
            ResultSet rs = selectUsedInventoriesIn.executeQuery();
            while(rs.next()){
                res.add(new UsedInventory(rs.getInt(1), rs.getInt(2), rs.getDouble(3)));
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;

    }

    public int deleteUsedInventory(int sId, int vId){
        try {
            deleteUsedInventory.setInt(1, sId);
            deleteUsedInventory.setInt(2, vId);
            deleteUsedInventory.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return 1;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertRecall(int sId, int vId){
        try {
            insertRecall.setInt(1, sId);
            insertRecall.setInt(2, vId);
            insertRecall.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public ArrayList<Integer>selectRecallsIn(int sId){
        ArrayList<Integer> res = new ArrayList<>();
        try {
            selectRecallsIn.setInt(1, sId);
            ResultSet rs = selectRecallsIn.executeQuery();
            while(rs.next()){
                res.add(rs.getInt(2));
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public UsedVehicle selectRecalledVehicle(int vId){
        UsedVehicle res = null;
        Vehicle v = selectVehicle(vId);
        if(v != null) {
            VType vt = selectVType(v.typeId);
            if(vt != null) {
                res = new UsedVehicle(v.vId, v.typeId, v.needMaintain, vt.model, vt.year, vt.options, -1.00);
            }
        }
        return res;
    }

    public int deleteRecallsAt(int sId) {
        try {
            deleteRecallsAt.setInt(1, sId);
            deleteRecallsAt.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public int recall(String model, int year, String options, ArrayList<Integer> serviceLocations){
        String typeId = model + year + options;
        try {
            getVIdWithTypeId.setString(1, typeId);
            ResultSet rs = getVIdWithTypeId.executeQuery();
            ArrayList<Integer> vIds = new ArrayList<>();
            while(rs.next()){
                vIds.add(rs.getInt(1));
            }
            for(int i = 0; i < vIds.size(); i ++){
                setOwnStatusPast.setInt(1, vIds.get(i));
                setOwnStatusPast.executeUpdate();
                insertRecall(serviceLocations.get((int)(Math.random() * serviceLocations.size())), vIds.get(i));
            }
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }

    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertShowroom(int sId, int vId){
        try {
            insertShowroom.setInt(1, sId);
            insertShowroom.setInt(2, vId);
            insertShowroom.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public ArrayList<UsedVehicle> selectShowroomAt(int sId) {
        ArrayList<UsedVehicle> res = new ArrayList<>();
        try {
            selectShowroomAt.setInt(1, sId);
            ResultSet rs = selectShowroomAt.executeQuery();
            while(rs.next()) {
                Vehicle v = selectVehicle(rs.getInt(2));
                if(v != null) {
                    VType vt = selectVType(v.typeId);
                    if(vt != null) {
                        res.add(new UsedVehicle(v.vId, v.typeId, "Showroom", vt.model, vt.year, vt.options, vt.salePrice));
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertNewDelivery(int sId, int vId, int id){
        try {
            insertNewDelivery.setInt(1, sId);
            insertNewDelivery.setInt(2, vId);
            insertNewDelivery.setInt(3, id);
            insertNewDelivery.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public int deleteNewDelivery(int sId, int vId, int id){
        try {
            deleteNewDelivery.setInt(1, sId);
            deleteNewDelivery.setInt(2, vId);
            deleteNewDelivery.setInt(3, id);
            deleteNewDelivery.executeUpdate();
            return 0;
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    public ArrayList<UsedVehicle> selectNewDeliveriesIn(int sId) {
        ArrayList<UsedVehicle> res = new ArrayList<>();
        try {
            selectNewDeliveriesIn.setInt(1, sId);
            ResultSet rs = selectNewDeliveriesIn.executeQuery();
            while(rs.next()) {
                Vehicle v = selectVehicle(rs.getInt(2));
                if(v != null) {
                    VType vt = selectVType(v.typeId);
                    if(vt != null) {
                        res.add(new UsedVehicle(v.vId, v.typeId, "NewDelivery", vt.model, vt.year, vt.options, vt.salePrice));
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public int selectNewDeliveredVId(int sId, int id){
        try {
            selectNewDeliveredVId.setInt(1, sId);
            selectNewDeliveredVId.setInt(2, id);
            ResultSet rs = selectNewDeliveredVId.executeQuery();
            if(rs.next()){
                return rs.getInt(1);
            }
            return -1;
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public int insertServiceVisit(int id, int vId, int sId, String description, String repairDate, double cost){
        int res = -1;
        try {
            insertServiceVisit.setInt(1, id);
            insertServiceVisit.setInt(2, vId);
            insertServiceVisit.setInt(3, sId);
            insertServiceVisit.setString(4, description);
            insertServiceVisit.setString(5, repairDate);
            insertServiceVisit.setDouble(6, cost);
            insertServiceVisit.executeUpdate();
            ResultSet rs = insertServiceVisit.getGeneratedKeys();
            if(rs.next()){
                res = rs.getInt(1);
            }
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public int insertPartialServiceVisit(int id, int vId, int sId){
        int res = -1;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDateTime now = LocalDateTime.now();
        String repairDate = dtf.format(now);
        try {
            insertPartialServiceVisit.setInt(1, id);
            insertPartialServiceVisit.setInt(2, vId);
            insertPartialServiceVisit.setInt(3, sId);
            insertPartialServiceVisit.setString(4, repairDate);
            insertPartialServiceVisit.executeUpdate();
            ResultSet rs = insertPartialServiceVisit.getGeneratedKeys();
            if(rs.next()){
                res = rs.getInt(1);
            }
        } catch(SQLException e){
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public ArrayList<ServiceVisit> selectPartialServiceVisit(int sId) {
        ArrayList<ServiceVisit> res = new ArrayList<>();
        try {
            selectPartialServiceVisit.setInt(1, sId);
            ResultSet rs = selectPartialServiceVisit.executeQuery();
            while(rs.next()) {
                res.add(new ServiceVisit(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getDouble(7)));
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

    public int handelPartialServiceVisit(int repairId, String description, double cost) {
        try {
            handelPartialServiceVisit.setString(1, description);
            handelPartialServiceVisit.setDouble(2, cost);
            handelPartialServiceVisit.setInt(3, repairId);
            handelPartialServiceVisit.executeUpdate();
            return 0;
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
            return -1;
        }

    }

    public ArrayList<ServiceVisit> selectFinishedServiceVisits(int sId) {
        ArrayList<ServiceVisit> res = new ArrayList<>();
        try {
            selectFinishedServiceVisitsIn.setInt(1, sId);
            ResultSet rs = selectFinishedServiceVisitsIn.executeQuery();
            while(rs.next()) {
                res.add(new ServiceVisit(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getDouble(7)));
            }
        } catch (SQLException e) {
            System.out.println("An error occured in our database. Please contact our technical assistant with the following error message.");
            e.printStackTrace();
        }
        return res;
    }

}
